use thefifthmemory;
# 用户表
drop table if exists sys_user;
create table sys_user
(
    id         bigint unsigned auto_increment comment '用户id',
    username       varchar(50) not null unique comment '用户名（账号）,目前使用的是邮箱',
    password       varchar(55) not null comment '密码',
    email           varchar(50) unique comment '邮箱',
    nick           varchar(100) comment '昵称',
    gender         tinyint unsigned default 1 comment '性别',
    avatar     varchar(255) not null comment '头像图片路径',
    active_code    varchar(20) not null comment '账户激活码',
    account_status tinyint unsigned default 0 comment '账户状态，0=未激活,1=激活,2=锁定',
    signature      varchar(255) comment '个人签名',
    gmt_create     timestamp not null default current_timestamp comment '创建时间',
    gmt_modified   timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id)
) engine = innodb
  charset utf8mb4 comment '用户表';

# 好友表
drop table if exists sys_friend;
create table sys_friend
(
    id         bigint unsigned auto_increment comment 'id',
    user_id   bigint unsigned not null comment '用户本人id',
    friend_id bigint unsigned not null comment '好友id,也是用户id',
    gmt_create     timestamp not null default current_timestamp comment '创建时间',
    gmt_modified   timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_friend (user_id)
) engine = innodb
  charset utf8mb4 comment = '好友表';

# 帖子表
drop table if exists sys_post;
create table sys_post
(
    id       bigint unsigned auto_increment comment '帖子id',
    title         varchar(255) comment '标题',
    user_id       bigint unsigned not null comment '帖子主人id',
    topic_id      bigint unsigned comment '话题id',
    like_count    bigint unsigned default 0 comment '点赞量',
    view_count    bigint unsigned default 0 comment '浏览量',
    comment_count bigint unsigned default 0 comment '评论量',
    post_state    tinyint            default 0 comment '帖子状态,0=草稿,1=待审核，2=审核通过发布，3=审核未通过',
    gmt_create     timestamp not null default current_timestamp comment '创建时间',
    gmt_modified   timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_user_id(user_id),
    index idx_topic_id(topic_id),
    index idx_create_time(gmt_create),
    index idx_count(like_count)
) engine = innodb
  charset utf8mb4 comment = '帖子表';

# 帖子图片表
drop table if exists sys_post_img;
create table sys_post_img
(
    id  bigint unsigned auto_increment comment '图片id',
    post_id bigint unsigned not null comment '帖子id',
    img_url varchar(255) not null comment '图片路径字符串',
    primary key (id),
    index idx_post_img (post_id)
) engine = innodb
  charset utf8mb4 comment = '帖子图片表';

# 帖子文字内容表
drop table if exists sys_post_text;
create table sys_post_text
(
    id   bigint unsigned not null comment 'id',
    post_id   bigint unsigned not null comment '帖子id',
    post_text text comment '帖子文本内容',
    primary key (id),
    index  idx_post_id(post_id)
) engine = innodb
  charset utf8mb4 comment = '帖子文字';


# 话题表
drop table if exists sys_topic;
create table sys_topic
(
    id     bigint unsigned auto_increment comment '话题id',
    topic_name   varchar(50) not null comment '话题名称',
    topic_img    varchar(255) not null comment '话题图标',
    gmt_create     timestamp not null default current_timestamp comment '创建时间',
    gmt_modified   timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id)
) engine = innodb
  charset utf8mb4 comment = '话题表';

# 评论表
drop table if exists sys_comment;
create table sys_comment
(
    id   bigint unsigned auto_increment comment '评论id',
    user_id      bigint unsigned not null comment '用户id',
    post_id      bigint unsigned not null comment '帖子id',
    comment_text varchar(255)    not null comment '评论内容',
    like_count   bigint unsigned default 0 comment '点赞量',
    reply_count  bigint unsigned default 0 comment '回复量',
    gmt_create     timestamp not null default current_timestamp comment '创建时间',
    gmt_modified   timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_comment(post_id)
) engine = innodb
  charset utf8mb4 comment = '评论表';

drop table if exists sys_reply;
create table sys_reply
(
    id         bigint unsigned auto_increment comment '回复表id',
    reply_id   bigint unsigned not null comment '回复的评论id',
    comment_id bigint unsigned not null comment '本评论id',
    gmt_create    timestamp not null default current_timestamp comment '创建时间',
    gmt_modified  timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_reply(comment_id)
) engine = innodb
  charset utf8mb4 comment = '回复表';

drop table if exists ams_role;
create table ams_role
(
    role_id   bigint unsigned auto_increment comment '角色id',
    role_name varchar(255) not null comment '角色名称',
    gmt_create    timestamp not null default current_timestamp comment '创建时间',
    gmt_modified  timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (role_id)
) engine = innodb
  charset utf8mb4 comment = '角色表';

drop table if exists ams_user_role;
create table ams_user_role
(
    id      bigint unsigned auto_increment comment '',
    user_id bigint unsigned not null comment '用户id',
    role_id bigint unsigned not null comment '角色id',
    gmt_create    timestamp not null default current_timestamp comment '创建时间',
    gmt_modified  timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_ur(user_id)
) engine = innodb
  charset utf8mb4 comment = '用户角色表';

drop table if exists ams_permission;
create table ams_permission
(
    id bigint unsigned auto_increment comment '权限id',
    permission_name varchar(100) not null,
    gmt_create    timestamp not null default current_timestamp comment '创建时间',
    gmt_modified  timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id)
) engine = innodb
  charset utf8mb4 comment = '权限表';

drop table if exists ams_role_permission;
create table ams_role_permission
(
    id           bigint unsigned auto_increment comment 'id',
    role_id      bigint unsigned not null comment '角色id',
    permission_id bigint unsigned not null comment '权限id',
    gmt_create    timestamp not null default current_timestamp comment '创建时间',
    gmt_modified  timestamp not null default current_timestamp on update  current_timestamp comment '最后修改时间',
    primary key (id),
    index idx_rp(role_id)
) engine = innodb
  charset utf8mb4 comment = '角色权限表';

