/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : thefifthmemory

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 11/07/2022 18:54:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ams_permission
-- ----------------------------
DROP TABLE IF EXISTS `ams_permission`;
CREATE TABLE `ams_permission`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `permission_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_permission
-- ----------------------------

-- ----------------------------
-- Table structure for ams_role
-- ----------------------------
DROP TABLE IF EXISTS `ams_role`;
CREATE TABLE `ams_role`  (
  `role_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_role
-- ----------------------------

-- ----------------------------
-- Table structure for ams_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `ams_role_permission`;
CREATE TABLE `ams_role_permission`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色id',
  `permission_id` bigint UNSIGNED NOT NULL COMMENT '权限id',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_rp`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for ams_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ams_user_role`;
CREATE TABLE `ams_user_role`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户id',
  `role_id` bigint UNSIGNED NOT NULL COMMENT '角色id',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_ur`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_comment
-- ----------------------------
DROP TABLE IF EXISTS `sys_comment`;
CREATE TABLE `sys_comment`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户id',
  `post_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '帖子id',
  `comment_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '评论内容',
  `like_count` bigint UNSIGNED NULL DEFAULT 0 COMMENT '点赞量',
  `reply_count` bigint UNSIGNED NULL DEFAULT 0 COMMENT '回复量',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_comment`(`post_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '评论表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_comment
-- ----------------------------
INSERT INTO `sys_comment` VALUES (41, 9, 64, '看到我你就不是第一了', 0, 0, '2022-06-29 15:28:04', '2022-06-29 15:28:04');
INSERT INTO `sys_comment` VALUES (42, 12, 76, '11111', 0, 0, '2022-06-29 15:53:43', '2022-06-29 15:53:43');
INSERT INTO `sys_comment` VALUES (43, 9, 104, '都看过来，绝对正品，味道绝佳', 0, 0, '2022-06-29 19:53:29', '2022-06-29 19:53:29');
INSERT INTO `sys_comment` VALUES (44, 15, 103, '很棒', 0, 0, '2022-06-29 19:53:57', '2022-06-29 19:53:57');
INSERT INTO `sys_comment` VALUES (45, 15, 103, '运动达人\n', 0, 0, '2022-06-29 19:54:10', '2022-06-29 19:54:10');
INSERT INTO `sys_comment` VALUES (46, 9, 104, '回头老顾客了都\n', 0, 0, '2022-06-29 19:54:44', '2022-06-29 19:54:44');
INSERT INTO `sys_comment` VALUES (47, 15, 105, '很有意义', 0, 0, '2022-06-29 19:54:54', '2022-06-29 19:54:54');
INSERT INTO `sys_comment` VALUES (48, 9, 103, '太强了哇', 0, 0, '2022-06-29 19:55:14', '2022-06-29 19:55:14');
INSERT INTO `sys_comment` VALUES (49, 9, 103, '看的我都激动了，明天就去', 0, 0, '2022-06-29 19:55:36', '2022-06-29 19:55:36');
INSERT INTO `sys_comment` VALUES (50, 9, 102, '好好好', 0, 0, '2022-06-29 19:55:57', '2022-06-29 19:55:57');
INSERT INTO `sys_comment` VALUES (51, 9, 102, '总归是能够发祝福了', 0, 0, '2022-06-29 19:56:11', '2022-06-29 19:56:11');
INSERT INTO `sys_comment` VALUES (52, 9, 101, '运动如人生', 0, 0, '2022-06-29 19:56:32', '2022-06-29 19:56:32');
INSERT INTO `sys_comment` VALUES (53, 9, 101, '我也热爱跑步', 0, 0, '2022-06-29 19:56:46', '2022-06-29 19:56:46');
INSERT INTO `sys_comment` VALUES (54, 9, 100, '每天都有不一样的心情', 0, 0, '2022-06-29 19:57:40', '2022-06-29 19:57:40');
INSERT INTO `sys_comment` VALUES (55, 9, 97, '哇塞，好好吃的亚子', 0, 0, '2022-06-29 19:58:27', '2022-06-29 19:58:27');
INSERT INTO `sys_comment` VALUES (56, 9, 96, '这个是什么食物，我怎么没吃过', 0, 0, '2022-06-29 19:59:03', '2022-06-29 19:59:03');
INSERT INTO `sys_comment` VALUES (57, 9, 96, '好想吃，馋死我了', 0, 0, '2022-06-29 19:59:25', '2022-06-29 19:59:25');
INSERT INTO `sys_comment` VALUES (58, 9, 95, '这个是真的真的真的非常好吃', 0, 0, '2022-06-29 20:00:03', '2022-06-29 20:00:03');
INSERT INTO `sys_comment` VALUES (59, 17, 105, '我也是这么认为的', 0, 0, '2022-06-29 20:00:52', '2022-06-29 20:00:52');
INSERT INTO `sys_comment` VALUES (60, 17, 104, '从没见过这么色香味俱全的牛肉干，强烈推荐', 0, 0, '2022-06-29 20:01:50', '2022-06-29 20:01:50');
INSERT INTO `sys_comment` VALUES (61, 17, 103, '这运动量快赶上我一年的了', 0, 0, '2022-06-29 20:02:33', '2022-06-29 20:02:33');
INSERT INTO `sys_comment` VALUES (62, 17, 102, '真的是佳句啊！', 0, 0, '2022-06-29 20:03:03', '2022-06-29 20:03:03');
INSERT INTO `sys_comment` VALUES (63, 17, 101, '我现在每天早上都会跑一段，为的是一个身体健康', 0, 0, '2022-06-29 20:04:07', '2022-06-29 20:04:07');
INSERT INTO `sys_comment` VALUES (64, 17, 92, '学校里的花花草草，只有仔细观赏时才能发现美', 0, 0, '2022-06-29 20:04:56', '2022-06-29 20:04:56');
INSERT INTO `sys_comment` VALUES (65, 17, 76, '这个钢铁侠好帅', 0, 0, '2022-06-29 20:05:25', '2022-06-29 20:05:25');
INSERT INTO `sys_comment` VALUES (66, 17, 75, '这是真的', 0, 0, '2022-06-29 20:05:46', '2022-06-29 20:05:46');
INSERT INTO `sys_comment` VALUES (67, 17, 91, '说的好（掌声~~~）', 0, 0, '2022-06-29 20:06:24', '2022-06-29 20:06:24');
INSERT INTO `sys_comment` VALUES (68, 17, 93, '有这么美的学校，我宁愿每天满课', 0, 0, '2022-06-29 20:07:09', '2022-06-29 20:07:09');
INSERT INTO `sys_comment` VALUES (69, 17, 99, 'come on', 0, 0, '2022-06-29 20:07:36', '2022-06-29 20:07:36');
INSERT INTO `sys_comment` VALUES (70, 17, 98, '哈哈哈，有毒，这个东西是不是全国统一的？', 0, 0, '2022-06-29 20:08:50', '2022-06-29 20:08:50');
INSERT INTO `sys_comment` VALUES (71, 18, 105, '哈哈哈哈', 0, 0, '2022-06-29 20:09:51', '2022-06-29 20:09:51');
INSERT INTO `sys_comment` VALUES (72, 18, 101, '很有意义', 0, 0, '2022-06-29 20:10:27', '2022-06-29 20:10:27');
INSERT INTO `sys_comment` VALUES (73, 18, 98, '我不理解，但我颇为震撼', 0, 0, '2022-06-29 20:11:10', '2022-06-29 20:11:10');
INSERT INTO `sys_comment` VALUES (74, 12, 102, '', 0, 0, '2022-06-29 20:57:08', '2022-06-29 20:57:08');
INSERT INTO `sys_comment` VALUES (75, 12, 102, '牛啊', 0, 0, '2022-06-29 20:57:16', '2022-06-29 20:57:16');
INSERT INTO `sys_comment` VALUES (76, 12, 105, '', 0, 0, '2022-06-29 20:58:10', '2022-06-29 20:58:10');
INSERT INTO `sys_comment` VALUES (77, 10, 101, '', 0, 0, '2022-06-29 20:59:22', '2022-06-29 20:59:22');
INSERT INTO `sys_comment` VALUES (78, 10, 101, '', 0, 0, '2022-06-29 20:59:42', '2022-06-29 20:59:42');
INSERT INTO `sys_comment` VALUES (79, 12, 104, '牛啊牛啊', 0, 0, '2022-06-29 21:07:31', '2022-06-29 21:07:31');
INSERT INTO `sys_comment` VALUES (80, 19, 106, '青春就是好啊！！！！！', 0, 0, '2022-06-30 09:50:21', '2022-06-30 09:50:21');
INSERT INTO `sys_comment` VALUES (81, 19, 107, '伤心的城市有个伤心的人', 0, 0, '2022-06-30 09:53:58', '2022-06-30 09:53:58');
INSERT INTO `sys_comment` VALUES (82, 12, 68, '1五恶化客情维护飞鸿过v家里的房间里', 0, 0, '2022-06-30 17:14:40', '2022-06-30 17:14:40');
INSERT INTO `sys_comment` VALUES (83, 12, 68, '嗡嗡嗡\n', 0, 0, '2022-06-30 17:16:38', '2022-06-30 17:16:38');
INSERT INTO `sys_comment` VALUES (84, 12, 68, '有有有由于', 0, 0, '2022-06-30 17:16:44', '2022-06-30 17:16:44');
INSERT INTO `sys_comment` VALUES (85, 10, 108, '真不错', 0, 0, '2022-06-30 17:16:49', '2022-06-30 17:16:49');
INSERT INTO `sys_comment` VALUES (86, 12, 68, '哇哇哇哇哇', 0, 0, '2022-06-30 17:16:53', '2022-06-30 17:16:53');
INSERT INTO `sys_comment` VALUES (87, 10, 108, '很好', 0, 0, '2022-06-30 17:16:57', '2022-06-30 17:16:57');
INSERT INTO `sys_comment` VALUES (88, 12, 68, '哇哇哇哇哇', 0, 0, '2022-06-30 17:16:57', '2022-06-30 17:16:57');
INSERT INTO `sys_comment` VALUES (89, 10, 105, '大连咖啡也不错', 0, 0, '2022-06-30 17:20:00', '2022-06-30 17:20:00');
INSERT INTO `sys_comment` VALUES (90, 12, 106, '是广东警方根据', 0, 0, '2022-06-30 17:20:33', '2022-06-30 17:20:33');
INSERT INTO `sys_comment` VALUES (91, 12, 106, '部佛教的护法经济', 0, 0, '2022-06-30 17:20:44', '2022-06-30 17:20:44');
INSERT INTO `sys_comment` VALUES (92, 21, 113, '只有健康是真正属于自己的', 0, 0, '2022-07-01 09:16:08', '2022-07-01 09:16:08');
INSERT INTO `sys_comment` VALUES (93, 9, 65, '五条老师真皮', 0, 0, '2022-07-01 09:48:30', '2022-07-01 09:48:30');
INSERT INTO `sys_comment` VALUES (94, 14, 135, '看起来好好吃', 0, 0, '2022-07-01 14:59:25', '2022-07-01 14:59:25');
INSERT INTO `sys_comment` VALUES (95, 14, 110, '哈哈哈', 0, 0, '2022-07-01 15:00:45', '2022-07-01 15:00:45');
INSERT INTO `sys_comment` VALUES (96, 14, 78, '大佬啊', 0, 0, '2022-07-01 15:04:43', '2022-07-01 15:04:43');
INSERT INTO `sys_comment` VALUES (97, 14, 106, '青春的记忆？', 0, 0, '2022-07-01 15:05:49', '2022-07-01 15:05:49');
INSERT INTO `sys_comment` VALUES (98, 9, 135, '好啊好啊', 0, 0, '2022-07-01 15:27:05', '2022-07-01 15:27:05');
INSERT INTO `sys_comment` VALUES (99, 9, 135, '好啊好啊', 0, 0, '2022-07-01 15:27:05', '2022-07-01 15:27:05');
INSERT INTO `sys_comment` VALUES (100, 18, 123, '这波狗粮吃的我猝不及防', 0, 0, '2022-07-01 15:42:29', '2022-07-01 15:42:29');
INSERT INTO `sys_comment` VALUES (101, 29, 123, '啊这', 0, 0, '2022-07-01 16:19:11', '2022-07-01 16:19:11');
INSERT INTO `sys_comment` VALUES (102, 30, 135, '想吃', 0, 0, '2022-07-01 16:36:14', '2022-07-01 16:36:14');
INSERT INTO `sys_comment` VALUES (103, 15, 0, '测试数据1', 0, 0, '2022-07-01 19:15:47', '2022-07-01 19:15:47');
INSERT INTO `sys_comment` VALUES (104, 15, NULL, '测试回复', 0, 0, '2022-07-01 19:21:19', '2022-07-01 19:21:19');
INSERT INTO `sys_comment` VALUES (105, 15, NULL, '测试回复1', 0, 0, '2022-07-01 19:29:42', '2022-07-01 19:29:42');
INSERT INTO `sys_comment` VALUES (106, 15, NULL, '测试回复2', 0, 0, '2022-07-01 19:29:48', '2022-07-01 19:29:48');
INSERT INTO `sys_comment` VALUES (107, 15, NULL, '测试回复3', 0, 0, '2022-07-01 19:29:55', '2022-07-01 19:29:55');
INSERT INTO `sys_comment` VALUES (108, 15, NULL, '测试回复1', 0, 0, '2022-07-01 19:30:03', '2022-07-01 19:30:03');
INSERT INTO `sys_comment` VALUES (109, 15, NULL, '测试回复2', 0, 0, '2022-07-01 19:30:08', '2022-07-01 19:30:08');
INSERT INTO `sys_comment` VALUES (110, 15, NULL, '测试回复3', 0, 0, '2022-07-01 19:30:13', '2022-07-01 19:30:13');
INSERT INTO `sys_comment` VALUES (111, 12, 135, '不错喔\n', 0, 0, '2022-07-01 20:55:49', '2022-07-01 20:55:49');
INSERT INTO `sys_comment` VALUES (112, 12, 111, '不错哦\n', 0, 0, '2022-07-02 09:05:07', '2022-07-02 09:05:07');
INSERT INTO `sys_comment` VALUES (113, 12, 111, '真的不错', 0, 0, '2022-07-02 09:05:23', '2022-07-02 09:05:23');
INSERT INTO `sys_comment` VALUES (114, 9, 113, '说的太好了！！！', 0, 0, '2022-07-02 09:10:30', '2022-07-02 09:10:30');
INSERT INTO `sys_comment` VALUES (115, 15, 138, '有道理', 0, 0, '2022-07-02 09:34:33', '2022-07-02 09:34:33');
INSERT INTO `sys_comment` VALUES (116, 15, 137, '对，就是该吃吃喝喝', 0, 0, '2022-07-02 09:34:51', '2022-07-02 09:34:51');
INSERT INTO `sys_comment` VALUES (117, 11, 138, '写的不错', 0, 0, '2022-07-02 10:07:26', '2022-07-02 10:07:26');
INSERT INTO `sys_comment` VALUES (118, 10, 146, '冯大哥飞洒发多少个人维生素', 0, 0, '2022-07-05 08:58:22', '2022-07-05 08:58:22');

-- ----------------------------
-- Table structure for sys_friend
-- ----------------------------
DROP TABLE IF EXISTS `sys_friend`;
CREATE TABLE `sys_friend`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint UNSIGNED NOT NULL COMMENT '用户本人id',
  `friend_id` bigint UNSIGNED NOT NULL COMMENT '好友id,也是用户id',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_friend`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '好友表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_friend
-- ----------------------------
INSERT INTO `sys_friend` VALUES (1, 15, 19, '2022-06-30 19:11:20', '2022-06-30 19:11:20');
INSERT INTO `sys_friend` VALUES (2, 19, 15, '2022-06-30 19:11:20', '2022-06-30 19:11:20');
INSERT INTO `sys_friend` VALUES (3, 15, 21, '2022-06-30 19:11:47', '2022-06-30 19:11:47');
INSERT INTO `sys_friend` VALUES (4, 21, 15, '2022-06-30 19:11:47', '2022-06-30 19:11:47');
INSERT INTO `sys_friend` VALUES (7, 15, 9, '2022-06-30 19:11:57', '2022-06-30 19:11:57');
INSERT INTO `sys_friend` VALUES (8, 9, 15, '2022-06-30 19:11:57', '2022-06-30 19:11:57');
INSERT INTO `sys_friend` VALUES (9, 12, 10, '2022-06-30 19:13:36', '2022-06-30 19:13:36');
INSERT INTO `sys_friend` VALUES (10, 10, 12, '2022-06-30 19:13:36', '2022-06-30 19:13:36');
INSERT INTO `sys_friend` VALUES (13, 9, 18, '2022-06-30 19:16:04', '2022-06-30 19:16:04');
INSERT INTO `sys_friend` VALUES (14, 18, 9, '2022-06-30 19:16:05', '2022-06-30 19:16:05');
INSERT INTO `sys_friend` VALUES (15, 17, 10, '2022-06-30 19:52:24', '2022-06-30 19:52:24');
INSERT INTO `sys_friend` VALUES (16, 10, 17, '2022-06-30 19:52:24', '2022-06-30 19:52:24');
INSERT INTO `sys_friend` VALUES (17, 18, 17, '2022-06-30 19:55:33', '2022-06-30 19:55:33');
INSERT INTO `sys_friend` VALUES (18, 17, 18, '2022-06-30 19:55:33', '2022-06-30 19:55:33');
INSERT INTO `sys_friend` VALUES (19, 18, 10, '2022-06-30 19:59:56', '2022-06-30 19:59:56');
INSERT INTO `sys_friend` VALUES (20, 10, 18, '2022-06-30 19:59:56', '2022-06-30 19:59:56');
INSERT INTO `sys_friend` VALUES (23, 16, 15, '2022-07-01 09:06:46', '2022-07-01 09:06:46');
INSERT INTO `sys_friend` VALUES (24, 15, 16, '2022-07-01 09:06:46', '2022-07-01 09:06:46');
INSERT INTO `sys_friend` VALUES (25, 10, 16, '2022-07-01 09:07:49', '2022-07-01 09:07:49');
INSERT INTO `sys_friend` VALUES (26, 16, 10, '2022-07-01 09:07:49', '2022-07-01 09:07:49');
INSERT INTO `sys_friend` VALUES (27, 17, 21, '2022-07-01 09:11:27', '2022-07-01 09:11:27');
INSERT INTO `sys_friend` VALUES (28, 21, 17, '2022-07-01 09:11:27', '2022-07-01 09:11:27');
INSERT INTO `sys_friend` VALUES (29, 27, 25, '2022-07-01 10:01:41', '2022-07-01 10:01:41');
INSERT INTO `sys_friend` VALUES (30, 25, 27, '2022-07-01 10:01:41', '2022-07-01 10:01:41');
INSERT INTO `sys_friend` VALUES (31, 26, 25, '2022-07-01 10:02:36', '2022-07-01 10:02:36');
INSERT INTO `sys_friend` VALUES (32, 25, 26, '2022-07-01 10:02:36', '2022-07-01 10:02:36');
INSERT INTO `sys_friend` VALUES (37, 10, 14, '2022-07-01 14:52:33', '2022-07-01 14:52:33');
INSERT INTO `sys_friend` VALUES (38, 14, 10, '2022-07-01 14:52:33', '2022-07-01 14:52:33');
INSERT INTO `sys_friend` VALUES (39, 29, 18, '2022-07-01 16:19:42', '2022-07-01 16:19:42');
INSERT INTO `sys_friend` VALUES (40, 18, 29, '2022-07-01 16:19:42', '2022-07-01 16:19:42');
INSERT INTO `sys_friend` VALUES (41, 30, 14, '2022-07-01 16:36:26', '2022-07-01 16:36:26');
INSERT INTO `sys_friend` VALUES (42, 14, 30, '2022-07-01 16:36:26', '2022-07-01 16:36:26');
INSERT INTO `sys_friend` VALUES (43, 16, 14, '2022-07-01 20:59:54', '2022-07-01 20:59:54');
INSERT INTO `sys_friend` VALUES (44, 14, 16, '2022-07-01 20:59:55', '2022-07-01 20:59:55');
INSERT INTO `sys_friend` VALUES (45, 16, 9, '2022-07-01 21:07:59', '2022-07-01 21:07:59');
INSERT INTO `sys_friend` VALUES (46, 9, 16, '2022-07-01 21:07:59', '2022-07-01 21:07:59');
INSERT INTO `sys_friend` VALUES (47, 11, 16, '2022-07-02 09:24:59', '2022-07-02 09:24:59');
INSERT INTO `sys_friend` VALUES (48, 16, 11, '2022-07-02 09:24:59', '2022-07-02 09:24:59');
INSERT INTO `sys_friend` VALUES (49, 11, 15, '2022-07-02 09:25:28', '2022-07-02 09:25:28');
INSERT INTO `sys_friend` VALUES (50, 15, 11, '2022-07-02 09:25:28', '2022-07-02 09:25:28');
INSERT INTO `sys_friend` VALUES (51, 11, 17, '2022-07-02 09:25:43', '2022-07-02 09:25:43');
INSERT INTO `sys_friend` VALUES (52, 17, 11, '2022-07-02 09:25:43', '2022-07-02 09:25:43');
INSERT INTO `sys_friend` VALUES (55, 27, 16, '2022-07-02 10:21:43', '2022-07-02 10:21:43');
INSERT INTO `sys_friend` VALUES (56, 16, 27, '2022-07-02 10:21:44', '2022-07-02 10:21:44');
INSERT INTO `sys_friend` VALUES (57, 10, 15, '2022-07-05 08:58:34', '2022-07-05 08:58:34');
INSERT INTO `sys_friend` VALUES (58, 15, 10, '2022-07-05 08:58:34', '2022-07-05 08:58:34');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '帖子id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `user_id` bigint UNSIGNED NOT NULL COMMENT '帖子主人id',
  `topic_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '话题id',
  `like_count` bigint UNSIGNED NULL DEFAULT 0 COMMENT '点赞量',
  `view_count` bigint UNSIGNED NULL DEFAULT 0 COMMENT '浏览量',
  `comment_count` bigint UNSIGNED NULL DEFAULT 0 COMMENT '评论量',
  `post_state` tinyint NULL DEFAULT 1 COMMENT '帖子状态,0=审核通过发布,1=待审核，2=草稿，3=审核未通过',
  `top` tinyint NULL DEFAULT 1 COMMENT '是否置顶,1=默认,2=置顶',
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帖子标签',
  `visible` tinyint NULL DEFAULT 1 COMMENT '帖子可见状态 1=公开 , 2=好友可见 , 3=仅自己可见 默认1',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id` ASC) USING BTREE,
  INDEX `idx_topic_id`(`topic_id` ASC) USING BTREE,
  INDEX `idx_create_time`(`gmt_create` ASC) USING BTREE,
  INDEX `idx_count`(`like_count` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '帖子表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (64, '第一条日记', 11, 4, 42, 0, 1, 0, 1, NULL, 1, '2022-06-29 15:24:38', '2022-06-29 18:17:12');
INSERT INTO `sys_post` VALUES (65, '天神', 12, 4, 214, 0, 1, 0, 1, NULL, 1, '2022-06-29 15:28:06', '2022-07-02 09:36:55');
INSERT INTO `sys_post` VALUES (67, '2022年06月29日记', 10, 1, 83, 0, 0, 0, 1, NULL, 1, '2022-06-29 15:30:17', '2022-06-30 09:31:02');
INSERT INTO `sys_post` VALUES (68, '校园', 13, 3, 1, 0, 5, 0, 1, NULL, 1, '2022-06-29 15:32:22', '2022-06-30 17:16:57');
INSERT INTO `sys_post` VALUES (69, '校园', 13, 3, 0, 0, 0, 0, 1, NULL, 1, '2022-06-29 15:33:20', '2022-06-29 15:33:20');
INSERT INTO `sys_post` VALUES (70, '校园', 13, 3, 0, 0, 0, 0, 1, NULL, 1, '2022-06-29 15:34:13', '2022-06-29 15:34:13');
INSERT INTO `sys_post` VALUES (75, '早餐', 9, 2, 8, 0, 1, 0, 1, NULL, 1, '2022-06-29 15:34:32', '2022-06-29 20:08:12');
INSERT INTO `sys_post` VALUES (76, '图片', 12, 3, 12, 0, 2, 0, 1, NULL, 1, '2022-06-29 15:37:52', '2022-06-29 20:05:24');
INSERT INTO `sys_post` VALUES (78, '2022年06月29日记', 10, 1, 13, 0, 1, 0, 1, NULL, 1, '2022-06-29 15:40:51', '2022-07-01 21:00:40');
INSERT INTO `sys_post` VALUES (79, '2022年06月29日记', 10, 1, 6, 0, 0, 0, 1, NULL, 1, '2022-06-29 15:55:23', '2022-06-29 16:00:58');
INSERT INTO `sys_post` VALUES (81, '2022年06月29日记', 10, 1, 4, 0, 0, 0, 1, NULL, 1, '2022-06-29 16:30:48', '2022-06-29 18:39:48');
INSERT INTO `sys_post` VALUES (91, '人生自由', 15, 1, 13, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:07:07', '2022-06-29 20:07:57');
INSERT INTO `sys_post` VALUES (92, '随手拍', 17, 3, 7, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:07:53', '2022-06-29 20:07:54');
INSERT INTO `sys_post` VALUES (93, '校园', 15, 3, 7, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:08:45', '2022-06-29 20:08:04');
INSERT INTO `sys_post` VALUES (94, '感悟', 15, 1, 8, 0, 0, 0, 1, NULL, 1, '2022-06-29 19:09:33', '2022-06-29 20:08:02');
INSERT INTO `sys_post` VALUES (95, '美食', 17, 2, 7, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:10:17', '2022-06-29 20:00:02');
INSERT INTO `sys_post` VALUES (96, '美食分享', 15, 2, 10, 0, 2, 0, 1, NULL, 1, '2022-06-29 19:10:46', '2022-06-29 20:07:23');
INSERT INTO `sys_post` VALUES (97, '美食', 15, 2, 7, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:11:26', '2022-06-29 20:07:21');
INSERT INTO `sys_post` VALUES (98, '家庭日常', 17, 1, 30, 0, 2, 0, 1, NULL, 1, '2022-06-29 19:12:17', '2022-07-01 09:16:53');
INSERT INTO `sys_post` VALUES (99, '运动', 16, 5, 6, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:12:36', '2022-06-29 20:07:36');
INSERT INTO `sys_post` VALUES (100, '打卡', 17, 4, 10, 0, 1, 0, 1, NULL, 1, '2022-06-29 19:15:14', '2022-07-01 16:37:44');
INSERT INTO `sys_post` VALUES (101, '运动', 16, 5, 82, 0, 6, 0, 1, NULL, 1, '2022-06-29 19:16:06', '2022-06-30 09:30:51');
INSERT INTO `sys_post` VALUES (102, '端午节佳句分享', 18, 6, 21, 0, 5, 0, 1, NULL, 1, '2022-06-29 19:19:50', '2022-06-29 20:57:16');
INSERT INTO `sys_post` VALUES (103, '2022年06月29日记', 18, 5, 22, 0, 5, 0, 1, NULL, 1, '2022-06-29 19:22:15', '2022-06-29 21:12:40');
INSERT INTO `sys_post` VALUES (104, '美食分享', 18, 2, 30, 0, 4, 0, 1, NULL, 1, '2022-06-29 19:24:50', '2022-07-01 09:55:21');
INSERT INTO `sys_post` VALUES (105, '运动', 9, 5, 46, 0, 5, 0, 1, NULL, 1, '2022-06-29 19:29:41', '2022-06-30 17:20:00');
INSERT INTO `sys_post` VALUES (106, '青春日记', 19, 3, 70, 0, 4, 0, 1, NULL, 1, '2022-06-30 09:43:34', '2022-07-01 15:05:48');
INSERT INTO `sys_post` VALUES (107, '美食', 20, 1, 52, 0, 1, 0, 1, NULL, 1, '2022-06-30 09:51:53', '2022-07-01 09:48:49');
INSERT INTO `sys_post` VALUES (108, '打卡', 21, 4, 33, 0, 2, 0, 1, NULL, 1, '2022-06-30 10:29:56', '2022-07-01 10:00:49');
INSERT INTO `sys_post` VALUES (109, '无敌', 12, 1, 11, 0, 0, 0, 1, NULL, 1, '2022-06-30 19:02:28', '2022-07-02 09:21:00');
INSERT INTO `sys_post` VALUES (110, '2022年06月30日记', 10, 1, 2, 0, 1, 0, 1, NULL, 1, '2022-06-30 19:02:53', '2022-07-01 15:00:45');
INSERT INTO `sys_post` VALUES (111, '宠物', 9, 1, 24, 0, 2, 0, 1, NULL, 1, '2022-06-30 19:03:37', '2022-07-02 09:05:22');
INSERT INTO `sys_post` VALUES (112, '美食分享', 9, 2, 46, 0, 0, 0, 1, NULL, 1, '2022-06-30 19:06:59', '2022-07-01 21:05:39');
INSERT INTO `sys_post` VALUES (113, '运动', 15, 5, 150, 0, 2, 0, 1, NULL, 1, '2022-06-30 19:07:52', '2022-07-02 09:10:30');
INSERT INTO `sys_post` VALUES (114, '运动', 15, 5, 0, 0, 0, 0, 1, NULL, 2, '2022-06-30 19:12:30', '2022-06-30 20:04:32');
INSERT INTO `sys_post` VALUES (115, '运动', 15, 5, 0, 0, 0, 0, 1, NULL, 3, '2022-06-30 19:13:00', '2022-07-01 09:02:45');
INSERT INTO `sys_post` VALUES (116, '日常', 17, 1, 1, 0, 0, 0, 1, NULL, 2, '2022-06-30 19:54:14', '2022-07-05 08:58:54');
INSERT INTO `sys_post` VALUES (117, '运动', 18, 5, 0, 0, 0, 0, 1, NULL, 2, '2022-06-30 20:02:39', '2022-07-01 22:32:03');
INSERT INTO `sys_post` VALUES (118, '校园', 18, 3, 0, 0, 0, 0, 1, NULL, 3, '2022-06-30 20:06:36', '2022-07-01 22:49:43');
INSERT INTO `sys_post` VALUES (119, '人生', 16, 1, 0, 0, 0, 0, 1, NULL, 3, '2022-07-01 09:04:43', '2022-07-01 22:53:16');
INSERT INTO `sys_post` VALUES (120, '锻炼', 17, 5, 0, 0, 0, 0, 1, NULL, 3, '2022-07-01 09:06:24', '2022-07-02 09:10:50');
INSERT INTO `sys_post` VALUES (121, '打卡', 18, 4, 0, 0, 0, 0, 1, NULL, 2, '2022-07-01 09:08:45', '2022-07-02 09:10:55');
INSERT INTO `sys_post` VALUES (122, '生活', 16, 1, 14, 0, 0, 0, 1, NULL, 1, '2022-07-01 09:14:04', '2022-07-02 09:20:27');
INSERT INTO `sys_post` VALUES (123, '打卡', 21, 4, 366, 0, 2, 0, 1, NULL, 1, '2022-07-01 09:14:11', '2022-07-02 09:39:41');
INSERT INTO `sys_post` VALUES (132, '校园', 9, 3, 0, 0, 0, 0, 1, NULL, 2, '2022-07-01 13:58:55', '2022-07-02 09:11:16');
INSERT INTO `sys_post` VALUES (133, '日常', 20, 1, 0, 0, 0, 0, 1, NULL, 3, '2022-07-01 14:14:37', '2022-07-02 09:07:22');
INSERT INTO `sys_post` VALUES (134, '心情', 14, 1, 0, 0, 0, 0, 1, NULL, 3, '2022-07-01 14:40:51', '2022-07-02 09:07:27');
INSERT INTO `sys_post` VALUES (135, '唯有美食不可辜负', 14, 2, 24, 0, 5, 0, 1, NULL, 1, '2022-07-01 14:48:43', '2022-07-02 11:07:02');
INSERT INTO `sys_post` VALUES (136, '美食', 16, 2, 14, 0, 0, 0, 1, NULL, 1, '2022-07-01 20:53:05', '2022-07-02 11:07:02');
INSERT INTO `sys_post` VALUES (137, '美食', 16, 2, 8, 0, 1, 0, 1, NULL, 1, '2022-07-01 20:54:23', '2022-07-02 11:07:03');
INSERT INTO `sys_post` VALUES (138, '生活', 16, 1, 6, 0, 2, 0, 1, NULL, 1, '2022-07-01 20:55:47', '2022-07-02 11:07:03');
INSERT INTO `sys_post` VALUES (139, '生活', 16, 1, 0, 0, 0, 0, 1, NULL, 1, '2022-07-01 20:59:44', '2022-07-02 11:07:03');
INSERT INTO `sys_post` VALUES (140, '1', 16, 4, 0, 0, 0, 0, 1, NULL, 2, '2022-07-01 21:05:41', '2022-07-02 11:07:03');
INSERT INTO `sys_post` VALUES (141, '萌宠', 9, 1, 0, 0, 0, 0, 1, NULL, 2, '2022-07-01 21:09:07', '2022-07-02 11:07:04');
INSERT INTO `sys_post` VALUES (142, '1', 15, 1, 0, 0, 0, 0, 1, NULL, 1, '2022-07-01 21:14:55', '2022-07-02 11:07:04');
INSERT INTO `sys_post` VALUES (143, '生活', 15, 1, 0, 0, 0, 0, 1, NULL, 1, '2022-07-01 21:16:20', '2022-07-02 11:07:04');
INSERT INTO `sys_post` VALUES (144, '生活', 15, 1, 0, 0, 0, 0, 1, NULL, 1, '2022-07-01 21:18:11', '2022-07-02 11:07:04');
INSERT INTO `sys_post` VALUES (145, '人生', 15, 1, 0, 0, 0, 1, 1, NULL, 1, '2022-07-01 21:18:49', '2022-07-05 09:01:08');
INSERT INTO `sys_post` VALUES (146, '老了', 15, 4, 1, 0, 1, 0, 1, NULL, 1, '2022-07-01 21:20:00', '2022-07-05 08:58:22');
INSERT INTO `sys_post` VALUES (147, '2022年07月02日记', 11, 1, 1, 0, 0, 1, 1, NULL, 1, '2022-07-02 10:09:59', '2022-07-05 09:01:07');
INSERT INTO `sys_post` VALUES (148, '2022年07月05日记', 10, 1, 0, 0, 0, 0, 1, NULL, 1, '2022-07-05 08:59:39', '2022-07-05 08:59:49');

-- ----------------------------
-- Table structure for sys_post_img
-- ----------------------------
DROP TABLE IF EXISTS `sys_post_img`;
CREATE TABLE `sys_post_img`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `post_id` bigint UNSIGNED NOT NULL COMMENT '帖子id',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片路径字符串',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_post_img`(`post_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 180 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '帖子图片表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post_img
-- ----------------------------
INSERT INTO `sys_post_img` VALUES (72, 64, '5e2ffc22-d748-4e43-9d6c-5b2dac811849.jpg');
INSERT INTO `sys_post_img` VALUES (73, 64, '4fe7cc76-b4a3-423e-80c1-e96180736bd2.png');
INSERT INTO `sys_post_img` VALUES (74, 64, 'c6e89e3d-b3bb-4a67-a6d8-304589d624b9.jpg');
INSERT INTO `sys_post_img` VALUES (75, 64, '0f3cad21-aabd-4b13-874b-767564f2e5b3.jpg');
INSERT INTO `sys_post_img` VALUES (76, 64, 'ca0f86fb-0168-479b-bb39-47c0078af385.jpg');
INSERT INTO `sys_post_img` VALUES (77, 65, 'a5e31dd5-73ab-424f-8e59-ccd43a23aeaf.jpg');
INSERT INTO `sys_post_img` VALUES (78, 67, 'e29b027d-86f1-4219-b8b0-30366ef90cc6.jpg');
INSERT INTO `sys_post_img` VALUES (79, 68, 'ca07372b-52f1-4a5f-ad66-72a59091ace6.jpg');
INSERT INTO `sys_post_img` VALUES (80, 68, 'deb7014b-0b24-476d-990a-d24bb6218445.jpg');
INSERT INTO `sys_post_img` VALUES (81, 69, 'c1abcb54-3083-49d2-9d35-7b4326fc0ee4.jpg');
INSERT INTO `sys_post_img` VALUES (82, 69, '7da5444d-5c2a-4207-b2ff-c6bc4598795a.png');
INSERT INTO `sys_post_img` VALUES (83, 70, '47b950b8-230d-46ad-9a1b-cfef9e135951.png');
INSERT INTO `sys_post_img` VALUES (84, 70, '84e503f5-9b00-4e10-b694-3cddfad5bedb.png');
INSERT INTO `sys_post_img` VALUES (85, 70, 'a2bc20c1-f7cb-4beb-a199-2bbed34e2b4a.jpg');
INSERT INTO `sys_post_img` VALUES (86, 75, 'a6ff25b0-2c9a-45cf-97a3-c70dc5805baf.png');
INSERT INTO `sys_post_img` VALUES (87, 76, 'c0d2bcda-d94d-4517-ac01-cc1fa2b14478.jpg');
INSERT INTO `sys_post_img` VALUES (88, 76, '875766b0-1e15-4009-b9fa-13b4df334e12.jpg');
INSERT INTO `sys_post_img` VALUES (89, 78, 'f087b5eb-6cef-4470-b745-a1129899775d.jpeg');
INSERT INTO `sys_post_img` VALUES (90, 78, 'e0a4d093-0180-416b-a39e-21c2f7452822.png');
INSERT INTO `sys_post_img` VALUES (91, 79, '3dc5d337-e3b1-434c-b815-0720027ee4c7.png');
INSERT INTO `sys_post_img` VALUES (92, 79, 'f7bf0444-26a0-406a-bee0-ea06dd7167fe.jpg');
INSERT INTO `sys_post_img` VALUES (93, 79, 'afbafee9-526b-42e9-b6f4-92d105499a51.jpg');
INSERT INTO `sys_post_img` VALUES (94, 79, 'a6e812ca-59b3-4bf1-bb91-617278f351aa.jpg');
INSERT INTO `sys_post_img` VALUES (95, 81, 'cb826b83-be15-4c11-a480-42f1fc62c3b4.jfif');
INSERT INTO `sys_post_img` VALUES (96, 81, '411074aa-42d5-4379-b51b-8a77b2002b57.png');
INSERT INTO `sys_post_img` VALUES (97, 81, '257fb2bb-0c8f-4da9-9cc9-9eb052cdc3d6.jpg');
INSERT INTO `sys_post_img` VALUES (98, 81, '1e303fd1-456d-4f6e-a478-80bfb9361abf.jpg');
INSERT INTO `sys_post_img` VALUES (99, 91, 'bb837448-5b9c-439c-b72d-524f97f75f27.jpg');
INSERT INTO `sys_post_img` VALUES (100, 92, '2b0b76b7-efe7-447a-a245-73d2e9f07a34.jpg');
INSERT INTO `sys_post_img` VALUES (101, 92, 'ded606f4-4a82-4e1a-ab12-7ed1ecb3ca4c.jpg');
INSERT INTO `sys_post_img` VALUES (102, 92, '0989ee89-e662-4c2f-a918-f308ee280dac.jpg');
INSERT INTO `sys_post_img` VALUES (103, 93, '0b9ac636-3291-432e-be34-ab396a07a130.jpg');
INSERT INTO `sys_post_img` VALUES (104, 94, 'f0b1883e-021a-487a-a547-86a07b1c2b15.jpg');
INSERT INTO `sys_post_img` VALUES (105, 95, 'b87650de-a7ba-483d-91ad-34e033e40a18.jpg');
INSERT INTO `sys_post_img` VALUES (106, 95, 'bb2bb970-2f61-4fc7-9a21-121930c6b24a.jpg');
INSERT INTO `sys_post_img` VALUES (107, 96, '94adaeda-70b6-49d5-8c8b-5156880475d8.jpg');
INSERT INTO `sys_post_img` VALUES (108, 96, '5b400782-09e4-4cda-88ef-7b86b8f441b2.jpg');
INSERT INTO `sys_post_img` VALUES (109, 97, '9503378b-1b59-4581-8911-7abd0f3c45b8.jpg');
INSERT INTO `sys_post_img` VALUES (110, 98, 'a096a319-90c2-46f3-b738-59e98b5a2a5a.png');
INSERT INTO `sys_post_img` VALUES (111, 98, '6c44c253-6075-4de5-9871-1451c6f50ad6.png');
INSERT INTO `sys_post_img` VALUES (112, 98, 'df4b0269-a880-453e-a2f3-af952da1335c.png');
INSERT INTO `sys_post_img` VALUES (113, 99, '2376e9aa-fa3b-48f8-b407-4d1a0208815b.jpg');
INSERT INTO `sys_post_img` VALUES (114, 100, '2b571560-dc1b-4578-9a97-27172083c536.jpg');
INSERT INTO `sys_post_img` VALUES (115, 101, '8702350d-3328-4471-b3fa-e8455cee1f83.jpg');
INSERT INTO `sys_post_img` VALUES (116, 102, 'd4f48d5a-3a69-4974-8048-5f5ef598c4be.jpg');
INSERT INTO `sys_post_img` VALUES (117, 102, '8a8e873a-aa92-48a7-9f81-5a100016c398.jpg');
INSERT INTO `sys_post_img` VALUES (118, 103, 'edeccd22-4381-4e36-899b-de9c8c608797.jpg');
INSERT INTO `sys_post_img` VALUES (119, 103, 'd981a465-dc0f-48c8-b0f2-ef85afc1d8ae.jpg');
INSERT INTO `sys_post_img` VALUES (120, 103, '10bb8a24-0a42-4cc0-b727-d979c5949f7a.jpg');
INSERT INTO `sys_post_img` VALUES (121, 104, '737947e0-7aa2-4192-9005-b5cfd85ebe44.jpg');
INSERT INTO `sys_post_img` VALUES (122, 104, '0f565a4b-c87c-4b82-9dd5-c1b7b69fd39c.jpg');
INSERT INTO `sys_post_img` VALUES (123, 104, '0953fb89-9d4e-4d56-b5c4-44c10ad921ed.jpg');
INSERT INTO `sys_post_img` VALUES (124, 105, '9169a05f-63bf-4c27-9c4c-08949fcfd9a9.jpg');
INSERT INTO `sys_post_img` VALUES (125, 106, '63cf2156-2183-4535-a145-13f68866bd74.png');
INSERT INTO `sys_post_img` VALUES (126, 106, '435975a3-c652-4bb7-821b-eb24a01ff977.png');
INSERT INTO `sys_post_img` VALUES (127, 107, 'a83ef7c6-47fa-485d-8491-e82722979393.webp');
INSERT INTO `sys_post_img` VALUES (128, 108, 'd47a9a82-065d-47b7-8a8e-37e664e602ba.jpg');
INSERT INTO `sys_post_img` VALUES (129, 109, '3acac598-ab9f-4b69-aa61-d85d6180ccdc.jpg');
INSERT INTO `sys_post_img` VALUES (130, 110, 'd6505b46-fa72-4b86-a3fe-ff099cacd9f5.jfif');
INSERT INTO `sys_post_img` VALUES (131, 111, '67f6a33d-5095-4a0c-94ea-5ed13b63bb1f.jpg');
INSERT INTO `sys_post_img` VALUES (132, 111, 'a33e7279-e9c5-42b8-8e02-515553565772.jpg');
INSERT INTO `sys_post_img` VALUES (133, 111, '74cadff0-732c-41df-8a48-ee3159e361d4.jpg');
INSERT INTO `sys_post_img` VALUES (134, 112, '7084e222-16bc-416d-8e27-25194bd163b3.jpg');
INSERT INTO `sys_post_img` VALUES (135, 112, 'd3d46d43-423a-47c3-825a-7af2a9e1c5e5.jpg');
INSERT INTO `sys_post_img` VALUES (136, 113, '3d95aece-6c2b-4934-a4ce-171a543fb9fb.jpg');
INSERT INTO `sys_post_img` VALUES (137, 114, 'b16fb890-b7e9-4ceb-8955-dca895833708.jpg');
INSERT INTO `sys_post_img` VALUES (138, 115, '6026bf01-1e9b-4402-9346-49d4d6c71ddf.jpg');
INSERT INTO `sys_post_img` VALUES (139, 116, '8511f5f6-e02d-46ee-a2d5-81b10ddc2e21.webp');
INSERT INTO `sys_post_img` VALUES (140, 117, '984ac733-b4eb-4f1f-be15-b962c8cb173b.jpg');
INSERT INTO `sys_post_img` VALUES (141, 118, '9452bcdf-1f97-47dd-8c64-dfbb1c659d31.jpg');
INSERT INTO `sys_post_img` VALUES (142, 118, 'abcd3318-a2e2-410a-90ce-edb779fdce42.jpg');
INSERT INTO `sys_post_img` VALUES (143, 118, '063cd684-b82e-4cf9-a80a-dca3d389ab4e.jpg');
INSERT INTO `sys_post_img` VALUES (144, 119, '5e02e4a7-0557-4de0-acb6-c1a3d342b17c.jpg');
INSERT INTO `sys_post_img` VALUES (145, 120, 'ee456297-6f0c-4029-bb94-a303d7722593.jpg');
INSERT INTO `sys_post_img` VALUES (146, 121, '9d1a5e31-d30d-4cfb-b9ef-8783025188f1.jpg');
INSERT INTO `sys_post_img` VALUES (147, 121, '0a6b8f03-dc37-4619-98e1-4102ba6bb48c.jpg');
INSERT INTO `sys_post_img` VALUES (148, 122, '37fb0352-9346-4cd0-9427-14fb8ac21e9f.jpg');
INSERT INTO `sys_post_img` VALUES (149, 123, '6d205ab5-c975-44de-98dc-1d1007c13838.jpg');
INSERT INTO `sys_post_img` VALUES (156, 132, 'aabbe6d7-79c3-419c-939d-4556f0dcf412.jpg');
INSERT INTO `sys_post_img` VALUES (157, 132, '343ab155-b718-4461-9d71-d22e27446b83.jpg');
INSERT INTO `sys_post_img` VALUES (158, 133, '04ea540a-2349-4a30-acb3-24edf2a11c20.jpg');
INSERT INTO `sys_post_img` VALUES (159, 133, '5ed766f8-dc39-4435-abab-651a7d49503c.jpg');
INSERT INTO `sys_post_img` VALUES (160, 133, '1c025144-727f-4473-8906-feea19ed114c.jpg');
INSERT INTO `sys_post_img` VALUES (161, 134, '8f433fb5-de63-42f3-a3a3-620fe93369a5.jpeg');
INSERT INTO `sys_post_img` VALUES (162, 134, '584779b7-17bf-47df-aad2-2aa9a4c9be0f.jpeg');
INSERT INTO `sys_post_img` VALUES (163, 134, '253f915d-a74c-41bf-9036-366c7957540d.jpeg');
INSERT INTO `sys_post_img` VALUES (164, 135, 'd45ca5e1-75f5-4e5d-be8f-65e85d5a54c6.jpeg');
INSERT INTO `sys_post_img` VALUES (165, 135, '7788d00d-3312-49e8-88dc-27dbfee51a04.webp');
INSERT INTO `sys_post_img` VALUES (166, 135, 'c9bfeeca-f05b-4289-9179-dae16d73d277.jpg');
INSERT INTO `sys_post_img` VALUES (167, 136, 'aaccb6da-f019-425d-b64c-9b21cfd6dd79.jpg');
INSERT INTO `sys_post_img` VALUES (168, 137, 'c5a28a3a-88e9-4b14-85f7-0a8e42200cd2.jpg');
INSERT INTO `sys_post_img` VALUES (169, 138, 'c7582952-349f-440b-b7a9-d52127da20cc.jpg');
INSERT INTO `sys_post_img` VALUES (170, 139, '2df71172-c529-495c-a170-79cd650e49dd.jpg');
INSERT INTO `sys_post_img` VALUES (171, 140, 'a476923d-40cc-4c00-8975-e417347026d6.jpg');
INSERT INTO `sys_post_img` VALUES (172, 141, '195905ee-cdce-4f13-97a4-24efbd09cdad.jpg');
INSERT INTO `sys_post_img` VALUES (173, 142, '6528dcb8-f11f-4353-a81f-c3f5f16c0bfd.jpg');
INSERT INTO `sys_post_img` VALUES (174, 143, 'a95a27f1-f35b-44cf-907d-82d72bcd2951.jpg');
INSERT INTO `sys_post_img` VALUES (175, 144, '146da6b7-74ff-4a1a-9cf6-bac634e91227.jpg');
INSERT INTO `sys_post_img` VALUES (176, 145, '07340239-88a4-4e9a-a404-27b7ee82b4dc.jpeg');
INSERT INTO `sys_post_img` VALUES (177, 146, '19eb93df-8568-42f3-8732-c18c18a59020.jpg');
INSERT INTO `sys_post_img` VALUES (178, 147, 'c978e193-6b84-4d45-b0ec-8375998074d3.png');
INSERT INTO `sys_post_img` VALUES (179, 148, 'de9e2121-1dd9-42a2-a9ba-14f927c4d379.jpg');

-- ----------------------------
-- Table structure for sys_post_text
-- ----------------------------
DROP TABLE IF EXISTS `sys_post_text`;
CREATE TABLE `sys_post_text`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `post_id` bigint UNSIGNED NOT NULL COMMENT '帖子id',
  `post_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '帖子文本内容',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_post_id`(`post_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '帖子文字' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post_text
-- ----------------------------
INSERT INTO `sys_post_text` VALUES (64, 64, '欢迎来到第五记忆，欢迎大家来这里分享关于你的故事。在这里你将遇到志同道合的好友！');
INSERT INTO `sys_post_text` VALUES (65, 65, '啊啊啊啊啊啊啊啊！你以为你接受的是谁的爱，是一个天神的爱！');
INSERT INTO `sys_post_text` VALUES (67, 67, '我是一行，欢迎来到第五记忆。');
INSERT INTO `sys_post_text` VALUES (68, 68, '当我第一次走进校门的那一刹那中，我发现最美的角落就是教室，琅琅的读书声在耳边回响，渐 渐它让我饱学知识，也交到新的朋友。 　　我喜欢这个地方的原因是它让我学习最喜欢的科目－自然和数学。自然是因我喜欢科学研究，而 且科学冒险也在生活里累积学习。\n');
INSERT INTO `sys_post_text` VALUES (69, 69, '我们的学校叫朱棣文小学，它有“园林校园”的美称。我最喜欢的校园一角就是读书角了。 　　漫步林荫道，在路的右侧，有一条曲折的长廊，这就是读书廊。在读书廊的柱子上，写着《三字 经》《弟子规》。诵着《三字经》。我对读书廊的喜爱就又多了一分。每年四月，紫藤花开了。');
INSERT INTO `sys_post_text` VALUES (70, 70, '我的校园坐落在宁静而美丽的聊大院校中，洋溢着浓浓的书香气，古朴典雅，却不失小学独有的 青春活泼的气息。 　　刚走进校园，一个崭新的塑胶操场便映入你眼帘。调皮好动的低年级同学常常跑来跑去，又不留 心脚下的路，摔倒就成了家常便饭。');
INSERT INTO `sys_post_text` VALUES (75, 75, '没有什么比一顿丰盛的早餐更让人心情愉悦！！！');
INSERT INTO `sys_post_text` VALUES (76, 76, '手办');
INSERT INTO `sys_post_text` VALUES (78, 78, '哈喽，我是一行，今天是我来到第五记忆的第一天。');
INSERT INTO `sys_post_text` VALUES (79, 79, '夕阳西下，断肠人在天涯。');
INSERT INTO `sys_post_text` VALUES (81, 81, '阿斯利康赶紧回来看还是\n杀戮空间和贵金属开个会\n爱上几个客户数据库');
INSERT INTO `sys_post_text` VALUES (91, 91, '不同的信念，决定不同的命运!');
INSERT INTO `sys_post_text` VALUES (92, 92, '校园一角');
INSERT INTO `sys_post_text` VALUES (93, 93, '　早上，突如其来的大雨伴着雷声，把我从梦中惊醒。我向外面一看，就惊呆了！外面乌云密布，对面的楼都看不见了。我心想：今天要不要上学呢？后来，天亮了雨停了，我就开开心心的上学了。');
INSERT INTO `sys_post_text` VALUES (94, 94, '累，你若储存的温暖越多，你的生活就会越阳光明媚');
INSERT INTO `sys_post_text` VALUES (95, 95, '杭城拌川届闻名的老店，茄汁拌川yyds\n家楼下发现开了一家，赶紧拿个首评🥰\n\n❤️ 流芳面馆\n\n🌟 茄汁拌川\n推荐指数：★★★★☆\n招牌茄汁拌川，番茄酱汁味道浓郁，裹上酱汁嗦一口面味道好极啦，里面有香干、韭黄、煎蛋，值得一夸的是煎蛋煎得很不错很香，和茄汁太配了\n\n🌟 腰花\n推荐指数：★★★★\n腰花q弹很嫩，味道也不错，略略有点腥味，另外洋葱太生了只是当作炒香的调料么😅建议洋葱炒熟更好吃\n\n🌟 虾仁\n推荐指数：★★★☆\n虾仁吃起来还是挺新鲜的，肉质还行有点弹性，也不怎么腥，没什么味道混在茄汁里还可以\n\n🌟 猪肝\n推荐指数：★★★☆\n猪肝有点太嫩了感觉没熟，味道不错很浓，洋葱同样建议炒熟\n\n💰RMB 23/👩🏻\n📍 绿城西溪世纪中心1号楼\n✏️ 新店环境还是整洁干净，不过大夏天的空调不太给力呀😂刚开有优惠周围的人都来吃了，店员有点少忙不过来');
INSERT INTO `sys_post_text` VALUES (96, 96, '日啖荔枝三百颗，不辞长作岭南人。');
INSERT INTO `sys_post_text` VALUES (97, 97, '美食当前，总能有所思，或馋性千娇，食前观察、吃中思想、品后体味');
INSERT INTO `sys_post_text` VALUES (98, 98, '常住人口和流动人口的区别！');
INSERT INTO `sys_post_text` VALUES (99, 99, '日复一日地坚持练下去吧，仅有活动适量才能坚持训练的热情和提高运动的技能。');
INSERT INTO `sys_post_text` VALUES (100, 100, '第845天#打卡#\n有案子可以接来单独负责了，好兴奋\n整理完证据清单和证据材料，写完答辩状后，一瞬间很想念烤猪蹄 ​ ');
INSERT INTO `sys_post_text` VALUES (101, 101, '有时候一旦你开始跑步，就只跑了分钟，便会顺其自然完成整段路线，并获得极大的满足感。当没有动力的时候，想想之前跑完每段路程后的喜悦及简便心境，你的犹豫就会烟消云散，开始只往前跑。');
INSERT INTO `sys_post_text` VALUES (102, 102, '有关端午节简单祝福文案 5月5发朋友圈说说句子分享！\n\n1.一度春秋，一岁芳华，端午又至，祈平安，祝健康。\n\n2.祝我们都安康，不止端午，也不止今夏。\n\n3.肆意放“粽”，奔赴山海，做最好的自己，端午节快乐！\n\n4.端午节，将思念细致包裹，让味道与记忆重逢，院子里炊烟升腾，带你拾野而归。\n\n5.端午，见传承之美，仲夏待优雅绽放\n\n6.端午小长假，粽情HAPPY，做快乐的自己。端午节快乐！\n\n7.粽叶飘香五月五，浓情端午共安康。\n\n8.高考遇端午，全上985。端午节之际，预祝高考顺利。\n\n9.为了内心的仪式感，吃个粽子，端午安康！\n\n10.将人间万物，包裹成生活的绚烂粽。人生百味做馅，皆因家的温馨变香甜。');
INSERT INTO `sys_post_text` VALUES (103, 103, '我的生活和感悟2022-摘星星✨星星⭐️\n纪念一下，摘星星的日子\n什么时候核酸改为72小时就更好了。\n雷雨来临的空隙跑一圈。');
INSERT INTO `sys_post_text` VALUES (104, 104, '爱吃牛肉干的家人们看过来！！\n内蒙特产牛肉干\n成份安全，无添加，孕妈妈也可以吃的哦！\n脂肪含量少，减肥中也可以吃！！！\n肉质细腻，口感好，有嚼劲，肉干干松！');
INSERT INTO `sys_post_text` VALUES (105, 105, '运动后分泌的多巴胺仅次于恋爱 所以来和我一起运动吧！');
INSERT INTO `sys_post_text` VALUES (106, 106, '青春的魅力');
INSERT INTO `sys_post_text` VALUES (107, 107, '我猜你饿了！');
INSERT INTO `sys_post_text` VALUES (108, 108, '白日烟火！\n打卡第74天。\n荣耀Magic4Pro');
INSERT INTO `sys_post_text` VALUES (109, 109, '打到我的人还没出生呢!哈哈哈啊哈哈');
INSERT INTO `sys_post_text` VALUES (110, 110, '6月月30日，心情大好。修了一天的bug。');
INSERT INTO `sys_post_text` VALUES (111, 111, '狗狗友好航班随便羡慕一下[苦涩]\n给各航司提一个不成熟的建议\n咱们有没有可能，在几个大城市之间安排宠物友好航班；每个月一班就行，两个月一班也行。\n保证你会发现生意好好[苦涩][苦涩][苦涩]\n\n各航司，你们不懂宠物人群，各种私人组织的包机活动一票难求，真的有市场！！！\n每月一班，是考虑到航司的成本问题，不喜欢狗的可以不选这班。喜欢宠物但自己没有宠物的可以选这个航班，同意带狗声明即可。\n\n小城市的也别急，有了大城市航班，周转再开车去小城市会比现在方便很多。大城市体量大，航司发现此路可行，可以再开更多。\n\n宠物上飞机的签署免责协议，自己管控自己的宠物，发生咬人事故自行负责。宠物需要单独买票。\n航司真的赚钱还无责，比托运狗，出了事天天被维权轻松多了。\n\n来嘛，试试嘛[跪了][跪了][跪了]\n真的，求求了');
INSERT INTO `sys_post_text` VALUES (112, 112, '喜市多这两个小蛋糕好好吃\n芋泥奶贝软dudu 抹茶巧克力蛋糕丝滑\n白象的老母鸡鸡汤面 加了肉滑跟焦丸 好吃');
INSERT INTO `sys_post_text` VALUES (113, 113, '没有一个朋友比得上健康，没有一个敌人比得上病魔，与其为病痛暗自流泪，不如运动健身\n为生命添彩。\n');
INSERT INTO `sys_post_text` VALUES (114, 114, '钻研书本是一种倦怠而微弱的运动，无所激动，然而交谈却立即教导与训练我们。');
INSERT INTO `sys_post_text` VALUES (115, 115, '吃得杂不用药，心境好百病消;动手动脑抗衰老，生活有度健康保。\n');
INSERT INTO `sys_post_text` VALUES (116, 116, '人生快不快乐看心情，幸不幸福看心态ɦǟքքʏ ɛʋɛʀʏɖǟʏ\n平凡小事促成的美好日常\n多吃不胖，积极又向上 ​');
INSERT INTO `sys_post_text` VALUES (117, 117, '最近……心情不太好。运动运动吧。');
INSERT INTO `sys_post_text` VALUES (118, 118, '[微风]景色这么好看，当然🉐拍照记录吧！[照相机]');
INSERT INTO `sys_post_text` VALUES (119, 119, '灰心生失望，失望生动摇，动摇生失败');
INSERT INTO `sys_post_text` VALUES (120, 120, '只要不断锻炼，我就是最强！');
INSERT INTO `sys_post_text` VALUES (121, 121, '#打卡# day49\n面包烤得有点糊，\n今天好累哦！');
INSERT INTO `sys_post_text` VALUES (122, 122, '人情冷暖正如花开花谢，不如将这种现象，想成一种必然的季节。');
INSERT INTO `sys_post_text` VALUES (123, 123, '今天是爱我家媳妇的第522天，今天的我又来郑州第六人民医院了起了一个大早也算是没迟到。 ​');
INSERT INTO `sys_post_text` VALUES (132, 132, '所剩无几的校园生活啦');
INSERT INTO `sys_post_text` VALUES (133, 133, '投稿！今天和朋友出来散步，遇到了三只耶耶！！！一只是成年耶，另外两只还是小猴耶🤣应该来自不同的主人。两只小猴耶的耳朵尾巴都被染色了😢不知道该说什么好…可能因为我们这是小地方，科学养狗意识亟待普及[单身狗]…希望所有家长都能爱护自己的宝贝，不要给宠物染色啦，染料对狗狗不好哦，耶耶原本白白胖胖的样子最可爱啦！');
INSERT INTO `sys_post_text` VALUES (134, 134, '啦啦啦');
INSERT INTO `sys_post_text` VALUES (135, 135, '海鲜大全');
INSERT INTO `sys_post_text` VALUES (136, 136, '美食当前，总能有所思，或馋性千娇，食前观察、吃中思想、品后体味。\n');
INSERT INTO `sys_post_text` VALUES (137, 137, '啥子体脂率，基础代谢率，这些在美食面前都是渣渣，人生就是吃吃喝喝。\n');
INSERT INTO `sys_post_text` VALUES (138, 138, '静静消失在人海里面，不让谁看见！想念是会呼吸的痛，瞬间蒸发，就让不可能的是也随至分散！');
INSERT INTO `sys_post_text` VALUES (139, 139, '跨越命运的栏杆，经历四季的更替。享受春的温暖、春的灿烂、春的明媚，抛弃夏的烦躁，让秋收\n的喜悦冲淡秋风的凄苦和孤寂，忍耐冬的寒冷，欣赏冰雪料峭的美丽。寒冬过后又是魅力无限的春天。\n');
INSERT INTO `sys_post_text` VALUES (140, 140, '好友看的');
INSERT INTO `sys_post_text` VALUES (141, 141, '做错事骂了他两句，他还委屈了');
INSERT INTO `sys_post_text` VALUES (142, 142, '笑着迎接每一天的目的是，无论遇到什么事儿都可以平常心，于生活也好，于工作也好，于他人也好。\n');
INSERT INTO `sys_post_text` VALUES (143, 143, '你不努力，永远不会有人对你公平。只有你努力了，有了资源，有了话语权以后，你才可能为自己争取公平的机会。\n所以，更加努力吧');
INSERT INTO `sys_post_text` VALUES (144, 144, '痛苦是黑暗中的摸索，前进的路途中满是坎坷');
INSERT INTO `sys_post_text` VALUES (145, 145, '寂寞的烟雨，总是那样的绵长。一滴泪顺着冰凉的脸颊滑落，终于明白：我步入了你的红尘，\n你嵌进了我的生命，曾经的咫尺，如今天各一方。这条路上，我的\'眼里还会浮现你的身影，而你也许\n已找不到我的频率。在风的世界，爱是一种等待；在雨的天地，爱是一种无奈。彼岸花开再美，叶落再\n悲壮，也无法欣赏，无法穿越');
INSERT INTO `sys_post_text` VALUES (146, 146, '年轻时候，以为坚持是永不动摇；到这个年纪，明白了，坚持就是犹疑着退缩着心猿意马着\n一步三停着，还在往前走。\n');
INSERT INTO `sys_post_text` VALUES (147, 147, '大家好');
INSERT INTO `sys_post_text` VALUES (148, 148, '一天的国风大赏鬼斧神工');

-- ----------------------------
-- Table structure for sys_reply
-- ----------------------------
DROP TABLE IF EXISTS `sys_reply`;
CREATE TABLE `sys_reply`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '回复表id',
  `reply_id` bigint UNSIGNED NOT NULL COMMENT '回复的评论id',
  `comment_id` bigint UNSIGNED NOT NULL COMMENT '本评论id',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_reply`(`comment_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '回复表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_reply
-- ----------------------------
INSERT INTO `sys_reply` VALUES (1, 104, 1, '2022-07-01 19:21:19', '2022-07-01 19:21:19');
INSERT INTO `sys_reply` VALUES (2, 105, 1, '2022-07-01 19:29:42', '2022-07-01 19:29:42');
INSERT INTO `sys_reply` VALUES (3, 106, 1, '2022-07-01 19:29:48', '2022-07-01 19:29:48');
INSERT INTO `sys_reply` VALUES (4, 107, 1, '2022-07-01 19:29:55', '2022-07-01 19:29:55');
INSERT INTO `sys_reply` VALUES (5, 108, 2, '2022-07-01 19:30:03', '2022-07-01 19:30:03');
INSERT INTO `sys_reply` VALUES (6, 109, 2, '2022-07-01 19:30:08', '2022-07-01 19:30:08');
INSERT INTO `sys_reply` VALUES (7, 110, 2, '2022-07-01 19:30:13', '2022-07-01 19:30:13');

-- ----------------------------
-- Table structure for sys_topic
-- ----------------------------
DROP TABLE IF EXISTS `sys_topic`;
CREATE TABLE `sys_topic`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '话题id',
  `topic_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '话题名称',
  `topic_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '话题图标',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `status` tinyint UNSIGNED NULL DEFAULT 1,
  `content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '话题表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_topic
-- ----------------------------
INSERT INTO `sys_topic` VALUES (1, '日常', 'daily.svg', '2022-06-25 15:09:42', '2022-07-01 21:47:25', 1, NULL);
INSERT INTO `sys_topic` VALUES (2, '美食', 'delicacy.svg', '2022-06-25 15:09:42', '2022-07-01 21:47:23', 1, NULL);
INSERT INTO `sys_topic` VALUES (3, '校园', 'campus.svg', '2022-06-25 15:09:42', '2022-07-01 21:47:15', 1, NULL);
INSERT INTO `sys_topic` VALUES (4, '打卡', 'punch.svg', '2022-06-25 15:09:42', '2022-07-01 21:47:27', 1, NULL);
INSERT INTO `sys_topic` VALUES (5, '运动', 'sport.svg', '2022-06-25 15:09:42', '2022-06-29 20:03:17', 1, NULL);
INSERT INTO `sys_topic` VALUES (6, '节日', 'festival.svg', '2022-06-25 15:09:42', '2022-06-29 20:03:17', 1, NULL);
INSERT INTO `sys_topic` VALUES (7, '权限管理测试', 'c76cfe59-3a5d-4d53-8191-62cfbc354429.jpg', '2022-07-01 21:33:30', '2022-07-02 09:45:00', 2, NULL);
INSERT INTO `sys_topic` VALUES (8, '游戏', 'ddffa7b2-762a-4e03-a0e9-e1a8a2e80f0b.png', '2022-07-02 09:12:54', '2022-07-02 10:14:16', 1, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名（账号）,目前使用的是邮箱',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `nick` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `gender` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '性别',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像图片路径',
  `active_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账户激活码',
  `account_status` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '账户状态，0=未激活,1=激活,2=锁定',
  `signature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '个人签名',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE,
  UNIQUE INDEX `email`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (9, '2415080861@qq.com', '$2a$10$ewIPgnzNQaWI7wCJwZCuYOcCk6jGuMlWXC66vw288IiH1LYjNNSoi', '2415080861@qq.com', '卖女孩的小火柴', 1, '13a3078e-f5b6-4123-8245-f09ca24b7848.jpg', '11e8b', 1, '这个家伙很懒，什么都没有留下', '2022-06-29 15:05:59', '2022-06-29 19:27:48');
INSERT INTO `sys_user` VALUES (10, '3350835072@qq.com', '$2a$10$nmKgz65KTV1LyrbRu9v2IeHJwlKWuscID7GudoXgJl4pamJEHNQ/i', '3350835072@qq.com', '一行', 1, '407ba192-5c1f-4960-9743-466574bcea23.jpg', '9ec44', 1, '海内存知己天涯若比邻。', '2022-06-29 15:08:21', '2022-06-30 16:49:13');
INSERT INTO `sys_user` VALUES (11, '3100610545@qq.com', '$2a$10$pDyeBDXcRcMC3Fs/ZXr0RuazcGRpbIx/RNbv9EMHMnwYbz9DS73L2', '3100610545@qq.com', '管理员1', 1, 'eb6f34c8-ca85-4340-b139-1fd18fcdad5d.jpg', '8c6d4', 2, '这个家伙很懒，什么都没有留下', '2022-06-29 15:12:05', '2022-07-02 10:14:42');
INSERT INTO `sys_user` VALUES (12, '2922340355@qq.com', '$2a$10$P2QnLdx0ykD2/WL9p3EffOKdJ8qVh9Mb57FxDd0A1Av1kMotkmhaG', '2922340355@qq.com', '风过', 1, '8fb0a662-01f0-42fc-b15e-9066d69da385.jpg', '306d6', 1, '自由不是随性所欲,而是自我掌控', '2022-06-29 15:15:32', '2022-07-01 15:35:28');
INSERT INTO `sys_user` VALUES (13, '421313518@qq.com', '$2a$10$cB26b0G4YGn.JpBSRTODcOIwK/5qLp5gG8q0F8KAhHEytZMU6M3py', '421313518@qq.com', '鞠天材', 1, 'a37bf866-ddb3-4ddb-9556-c55a1c345699.jpg', 'b7982', 1, '这个家伙很懒，什么都没有留下', '2022-06-29 15:30:33', '2022-06-29 15:31:43');
INSERT INTO `sys_user` VALUES (14, '2188396528@qq.com', '$2a$10$t2Y/JjfUNiTTxKQPl9ZHMuIlO3i9BTHZmUvbT.t1znC07wjhRIqNa', '2188396528@qq.com', '1234', 2, 'b601b8d5-eca0-4a5b-9cfa-91bcac0f7447.jpeg', '8064b', 1, '111111', '2022-06-29 15:36:51', '2022-07-01 14:49:44');
INSERT INTO `sys_user` VALUES (15, '123456@qq.com', '$2a$10$t2Y/JjfUNiTTxKQPl9ZHMuIlO3i9BTHZmUvbT.t1znC07wjhRIqNa', '123456@qq.com', '谷梁凯乐', 1, '110632a4-6ee3-4309-9037-cb21e6411dd8.jpg', '8064b', 1, '这个家伙很懒，什么都没有留下', '2022-06-29 15:36:51', '2022-06-29 19:06:08');
INSERT INTO `sys_user` VALUES (16, '123457@qq.com', '$2a$10$0wUGg7VOTK8ARGtbsSOqV.z2gkaL9gCKqxur9Q9ijGezf1pix2A2S', '123457@qq.com', '史珍祥', 1, '7d1d0391-c900-4c19-9237-f02c94e82b04.jpg', '8064b', 1, '这个家伙很懒，什么都没有留下', '2022-06-29 15:36:51', '2022-07-01 21:10:00');
INSERT INTO `sys_user` VALUES (17, '123458@qq.com', '$2a$10$t2Y/JjfUNiTTxKQPl9ZHMuIlO3i9BTHZmUvbT.t1znC07wjhRIqNa', '123458@qq.com', '海角', 1, '08b0102d-4bce-4d33-bbe0-b97ad4cac26f.jpeg', '8064b', 1, '我虽浪迹天涯', '2022-06-29 15:36:51', '2022-06-29 19:05:38');
INSERT INTO `sys_user` VALUES (18, '123459@qq.com', '$2a$10$t2Y/JjfUNiTTxKQPl9ZHMuIlO3i9BTHZmUvbT.t1znC07wjhRIqNa', '123459@qq.com', 'bang~', 2, '8e66ff3a-e69c-4660-8f3c-02a73f94960d.jpg', '8064b', 1, '乘风破浪', '2022-06-29 15:36:51', '2022-06-29 19:17:49');
INSERT INTO `sys_user` VALUES (19, '1157581303@qq.com', '$2a$10$GGSLNrtqxbUlKDMiX3yEKuCoulmqAY6f.q/Mq5UrA6yyx/ZpBbKiq', '1157581303@qq.com', '老吴不老', 1, '1ece14f9-4966-43ad-b844-78f398d605e5.jpg', '53085', 1, '并无个性，何须签名', '2022-06-30 09:28:14', '2022-06-30 09:39:38');
INSERT INTO `sys_user` VALUES (20, '198457791@qq.com', '$2a$10$Sla.ZuJ1wbE8OWIU3XuOLuR7HV/JuYtk4Ps1mH0BP9bNukgR51Qia', '198457791@qq.com', '深深太平洋里深深伤心', 1, 'ea1fb855-e8b2-489f-8836-7c7e8534ebf1.jpg', '6b9a1', 1, '这个家伙很懒，什么都没有留下', '2022-06-30 09:45:29', '2022-07-01 14:16:14');
INSERT INTO `sys_user` VALUES (21, '1059204500@qq.com', '$2a$10$IX5Kdk0W6KI2WiuI0R0ufuKBsnvswVHcoImBPp6dkX7rU4fXRycuq', '1059204500@qq.com', '独食侠', 1, '9907ccc5-0a72-4f79-8c05-3a481ed4771b.jpg', '9299f', 1, '这个家伙很懒，什么都没有留下', '2022-06-30 10:26:22', '2022-06-30 10:27:50');
INSERT INTO `sys_user` VALUES (22, '2669491232@qq.com', '$2a$10$QOcRRTJorQhcCJ8f7jZ8UOV4IX4ze.uFCE960tnS2lJg.Ckld1MmS', '2669491232@qq.com', 'abc123', 1, 'header_default.jpg', '24e2e', 1, '这个家伙很懒，什么都没有留下', '2022-06-30 19:02:30', '2022-07-01 21:51:21');
INSERT INTO `sys_user` VALUES (23, '2761804524@qq.com', '$2a$10$I4q4jVahvv0MmrPEof2WvuuHp4VupDuozwxHpxCuAutTGTaTtzzxm', '2761804524@qq.com', '陶辉的爸爸', 1, '6ae6415c-0543-4cfc-bf35-f8da836ad284.png', 'd5d2b', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:50:02', '2022-07-02 11:03:34');
INSERT INTO `sys_user` VALUES (24, '1419593170@qq.com', '$2a$10$m4V831bdmY3tLXS7sseqruXqqpn1CI92eTPpv1PwzRi8OGfdN.iM.', '1419593170@qq.com', 'hhh', 1, 'header_default.jpg', 'd225d', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:51:53', '2022-07-01 09:51:53');
INSERT INTO `sys_user` VALUES (25, '1085113010@qq.com', '$2a$10$H5gSMzxNV6Kn38VsoQyp2esd.0omDyYu8aVQdGw/FC66rcq43/cja', '1085113010@qq.com', 'th1085113010', 1, 'header_default.jpg', 'aad3a', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:53:40', '2022-07-01 09:53:40');
INSERT INTO `sys_user` VALUES (26, '2548691614@qq.com', '$2a$10$jREhN4rCfngkr8JzJW0ffeIJ96PyS53/Gex7Q2V0y2.GTjNV8/2pi', '2548691614@qq.com', '爸爸', 1, 'header_default.jpg', 'e2ae4', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:56:47', '2022-07-02 11:03:38');
INSERT INTO `sys_user` VALUES (27, '2390701167@qq.com', '$2a$10$uzvAcrN1KUnAnGlXfTHJZ.xXysIlkByaehUwWoLFmWQf6TriR45n.', '2390701167@qq.com', 'niuniu', 1, '49888d52-ce94-4ecc-a387-7f04e8f489ca.jpg', '91c94', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:58:27', '2022-07-02 10:16:49');
INSERT INTO `sys_user` VALUES (28, '503061365@qq.com', '$2a$10$Og9lqaVii8lu74ZSRIKbeOkxRZm1fftroR4q5.q3wl1242gRULBlK', '503061365@qq.com', 'aeae', 1, '483884c5-9536-4349-b98a-4a0b5b01fb01.jpg', 'a2491', 1, '这个家伙很懒，什么都没有留下', '2022-07-01 09:59:10', '2022-07-01 10:00:10');
INSERT INTO `sys_user` VALUES (29, '1878299511@qq.com', '$2a$10$smzpe2poaNGJTu2Xb0PlCubk7ugwXKVS44JezCPzi56nZQPKvCn0m', '1878299511@qq.com', '胖虎', 2, '70b119b2-8a19-4235-b6c4-693b64435cb2.jpg', '28924', 1, '我叫胖虎', '2022-07-01 16:17:35', '2022-07-01 16:18:34');
INSERT INTO `sys_user` VALUES (30, '1074357389@qq.com', '$2a$10$h/FqhHm3SPOlSL4np9pp/O/zkfoB3o0zl2b/lG7AspSiBuIMi7Z1C', '1074357389@qq.com', 'success', 1, '2ef091b6-7e0c-4b51-ad7e-6e89929c544b.jpg', 'eac52', 1, 'keke', '2022-07-01 16:34:28', '2022-07-02 11:03:29');

SET FOREIGN_KEY_CHECKS = 1;
