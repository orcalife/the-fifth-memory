/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : blog_ams

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 11/07/2022 18:54:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ams_admin
-- ----------------------------
DROP TABLE IF EXISTS `ams_admin`;
CREATE TABLE `ams_admin`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码（密文）',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像url',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `is_enable` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否启用，1=启用，0=未启用',
  `last_login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后登录ip地址（冗余）',
  `login_count` int UNSIGNED NULL DEFAULT 0 COMMENT '累计登录次数（冗余）',
  `gmt_last_login` datetime NULL DEFAULT NULL COMMENT '最后登录时间（冗余）',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE,
  UNIQUE INDEX `phone`(`phone` ASC) USING BTREE,
  UNIQUE INDEX `email`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_admin
-- ----------------------------
INSERT INTO `ams_admin` VALUES (1, 'min_admin', '$2a$10$xdaGc8l4uTYTPuBxRFcMdev/Xbi0YRtUUWC4wKYhoRIazh/b20u56', '初级管理员', NULL, NULL, 'root@tedu.cn', '最高管理员', 1, NULL, 0, NULL, NULL, NULL);
INSERT INTO `ams_admin` VALUES (2, 'super_admin', '$2a$10$xdaGc8l4uTYTPuBxRFcMdev/Xbi0YRtUUWC4wKYhoRIazh/b20u56', '超级管理员', NULL, NULL, 'admin@tedu.cn', '超级管理员', 1, NULL, 0, NULL, NULL, NULL);
INSERT INTO `ams_admin` VALUES (3, 'normal_admin', '$2a$10$xdaGc8l4uTYTPuBxRFcMdev/Xbi0YRtUUWC4wKYhoRIazh/b20u56', '普通管理员', NULL, NULL, 'liucs@tedu.cn', NULL, 0, NULL, 0, NULL, NULL, NULL);
INSERT INTO `ams_admin` VALUES (4, 'sys_admin', '$2a$10$xdaGc8l4uTYTPuBxRFcMdev/Xbi0YRtUUWC4wKYhoRIazh/b20u56', '系统管理员', NULL, NULL, 'lxr@tedo.cn', '超级管理员', 1, NULL, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ams_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `ams_admin_role`;
CREATE TABLE `ams_admin_role`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '管理员id',
  `role_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '角色id',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '管理员角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_admin_role
-- ----------------------------
INSERT INTO `ams_admin_role` VALUES (1, 1, 3, NULL, NULL);
INSERT INTO `ams_admin_role` VALUES (4, 2, 1, NULL, NULL);
INSERT INTO `ams_admin_role` VALUES (6, 3, 4, NULL, NULL);
INSERT INTO `ams_admin_role` VALUES (7, 4, 2, NULL, NULL);

-- ----------------------------
-- Table structure for ams_login_log
-- ----------------------------
DROP TABLE IF EXISTS `ams_login_log`;
CREATE TABLE `ams_login_log`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '管理员id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员用户名（冗余）',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员昵称（冗余）',
  `ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录ip地址',
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '浏览器内核',
  `gmt_login` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '管理员登录日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_login_log
-- ----------------------------
INSERT INTO `ams_login_log` VALUES (1, 1, 'root', 'root', '127.0.0.1', 'mozilla/5.0 (macintosh; intel mac os x 10_15_7) applewebkit/605.1.15 (khtml, like gecko) version/15.0 safari/605.1.15', '2022-06-17 09:53:35', NULL, NULL);
INSERT INTO `ams_login_log` VALUES (2, 2, 'root', 'root', '127.0.0.1', 'mozilla/5.0 (macintosh; intel mac os x 10_15_7) applewebkit/605.1.15 (khtml, like gecko) version/15.0 safari/605.1.15', '2022-06-17 21:53:35', NULL, NULL);
INSERT INTO `ams_login_log` VALUES (3, 3, 'root', 'root', '127.0.0.1', 'mozilla/5.0 (macintosh; intel mac os x 10_15_7) applewebkit/605.1.15 (khtml, like gecko) version/15.0 safari/605.1.15', '2022-06-18 09:53:35', NULL, NULL);

-- ----------------------------
-- Table structure for ams_permission
-- ----------------------------
DROP TABLE IF EXISTS `ams_permission`;
CREATE TABLE `ams_permission`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '值',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '自定义排序序号',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_permission
-- ----------------------------
INSERT INTO `ams_permission` VALUES (1, 'view', '/admin', '浏览后台权限', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (2, 'addTopic', '/topic/addNewTopic', '添加话题权限', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (3, 'checkPost', '/post/check', '审核帖子权限', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (4, 'deletePosst', '/post/delete', '删除帖子权限', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (5, 'setUserStatus', '/user/setUserStatus', '启用禁用账号', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (6, 'updateTopic', '/topic/updateTopic', '编辑话题', 0, NULL, NULL);
INSERT INTO `ams_permission` VALUES (7, 'setTopicStatus', 'topic/setTopicStatus', '启用禁用话题', 0, NULL, NULL);

-- ----------------------------
-- Table structure for ams_role
-- ----------------------------
DROP TABLE IF EXISTS `ams_role`;
CREATE TABLE `ams_role`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '自定义排序序号',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_role
-- ----------------------------
INSERT INTO `ams_role` VALUES (1, '超级管理员', '所有权限', 0, NULL, NULL);
INSERT INTO `ams_role` VALUES (2, '系统管理员', '审核帖子权限', 0, NULL, NULL);
INSERT INTO `ams_role` VALUES (3, '初級管理员', '添加话题权限', 0, NULL, NULL);
INSERT INTO `ams_role` VALUES (4, '普通管理员', '只有浏览权限', 0, NULL, NULL);

-- ----------------------------
-- Table structure for ams_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `ams_role_permission`;
CREATE TABLE `ams_role_permission`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '权限id',
  `gmt_create` datetime NULL DEFAULT NULL COMMENT '数据创建时间',
  `gmt_modified` datetime NULL DEFAULT NULL COMMENT '数据最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色权限关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ams_role_permission
-- ----------------------------
INSERT INTO `ams_role_permission` VALUES (1, 1, 1, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (2, 2, 3, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (3, 3, 2, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (4, 4, 1, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (5, 1, 2, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (6, 1, 3, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (7, 1, 4, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (8, 1, 5, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (9, 3, 6, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (10, 3, 7, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (11, 1, 6, NULL, NULL);
INSERT INTO `ams_role_permission` VALUES (12, 1, 7, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
