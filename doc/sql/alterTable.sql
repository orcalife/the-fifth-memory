use thefifthmemory;
# 该字段表示话题状态，1--默认启用，0--不启用
alter table sys_topic add status tinyint default 1;

# 说明：访问后台的时候，URL路径为/admin/模块名
/*create table ams_administrator(
    id bigint unsigned auto_increment,
    username varchar(50) not null comment '管理员账号',
    password varchar(100) not null comment '密码',
    primary key(id)
)engine=innodb
charset utf8mb4 comment '管理员表';

create table ams_admin_role(
    id bigint unsigned auto_increment,
    administrator_id bigint unsigned not null comment '管理员id',
    role_id bigint unsigned not null comment '管理员角色',
    primary key (id)
)engine=innodb
 charset utf8mb4 comment '管理员角色表';*/

# 评论的post_id默认为null，表示该评论是一条回复其他评论的评论.
# 不为null的时候表示某帖子增加一条新的评论。
use thefifthmemory;
alter table sys_comment change post_id post_id bigint unsigned default null comment '帖子id';

alter table ams_role change role_id id bigint unsigned auto_increment;

alter table sys_user change password password varchar(255) not null comment '密码';

alter table sys_post_text change id id bigint unsigned auto_increment comment 'id'
use thefifthmemory;
alter table sys_topic add status tinyint unsigned default 1;

alter table sys_post add top tinyint default 1 comment '是否置顶,1=默认不置顶,2=置顶';

alter table sys_post add label varchar(50) default null comment '帖子标签';
alter table sys_post add visible tinyint default 1 comment '帖子可见状态 1=公开 , 2=好友可见 , 3=仅自己可见 默认为1';

alter table sys_post change post_state post_state tinyint default 1 comment '帖子状态,0=审核通过发布,1=待审核，2=草稿，3=审核未通过';
alter table sys_topic add content varchar(100) comment '话题描述';
