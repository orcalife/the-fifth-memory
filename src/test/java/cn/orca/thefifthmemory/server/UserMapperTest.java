package cn.orca.thefifthmemory.server;

import cn.orca.thefifthmemory.server.entity.User;
import cn.orca.thefifthmemory.server.mapper.common.UserMapper;
import cn.orca.thefifthmemory.server.pojo.vo.UserLoginVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;


    @Test
    void insertTest(){
        User user = new User();
        user.setUsername("3350835072@qq.com");
        user.setEmail("3350835072@qq.com");
        user.setPassword("abc123");
        user.setNick("一行管理员");
        user.setActiveCode("b34d23");
        user.setAvatar("/img/");
        user.setAccountStatus(1);
        userMapper.insert(user);
    }

    @Test
    void queryUsernameTest(){
        UserLoginVO userLoginVO = userMapper.queryUsername("3350835072@qq.com");
        System.out.println(userLoginVO);
    }

    @Test
    void queryUserSimpleInfoTest(){
        System.out.println(userMapper.queryUserSimpleInfoById(2L));
    }

}
