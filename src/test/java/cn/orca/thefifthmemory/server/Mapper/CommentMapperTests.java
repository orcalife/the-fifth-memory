package cn.orca.thefifthmemory.server.Mapper;

import cn.orca.thefifthmemory.server.entity.Comment;
import cn.orca.thefifthmemory.server.mapper.common.CommentMapper;
import cn.orca.thefifthmemory.server.pojo.vo.CommentSimpleVO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

@SpringBootTest
public class CommentMapperTests {
    @Autowired
    CommentMapper commentMapper;
    @Sql({"classpath:truncate.sql","classpath:insert_data.sql"})
    @Test
    public void testListComment(){
        List<CommentSimpleVO> commentSimpleVOList=commentMapper.listAllComments(1L);
        Assertions.assertNotNull(commentSimpleVOList);
        System.out.println(commentSimpleVOList);
    }
    @Sql({"classpath:truncate.sql","classpath:insert_data.sql"})
    @Test
    public void testInsertComment(){
        Comment comment=new Comment();
        comment.setCommentText("测试数据1");
        comment.setPostId(1L);
        comment.setUserId(1L);
        comment.setLikeCount(100L);
        Assertions.assertDoesNotThrow(()->{
            commentMapper.insertComment(comment);
        });


    }
}
