package cn.orca.thefifthmemory.server.Mapper;


import cn.orca.thefifthmemory.server.entity.Reply;
import cn.orca.thefifthmemory.server.mapper.common.ReplyMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
public class ReplyMapperTests {
    @Autowired
    ReplyMapper replyMapper;
    @Sql("classpath:truncate_reply.sql")
    @Test
    public  void addReplyTests(){
        Reply reply=new Reply();
        reply.setReplyId(1L);
        reply.setCommentId(2L);
        int rows=replyMapper.addReply(reply);
        Assertions.assertEquals(rows,1);
    }
    @Test
    public  void deleteReplyByIdTests(){
        int rows=replyMapper.deleteReplyById(1L);
        Assertions.assertEquals(rows,1);
    }

}
