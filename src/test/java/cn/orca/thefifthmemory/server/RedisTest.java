package cn.orca.thefifthmemory.server;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;

@SpringBootTest
public class RedisTest {
    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Test
    void simpleTest(){
        redisTemplate.opsForValue().set("测试1","大家好");
    }
}
