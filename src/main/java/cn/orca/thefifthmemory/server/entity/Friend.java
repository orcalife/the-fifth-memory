package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Friend  implements Serializable {
	private static final long serialVersionUID =  6921241368151337354L;

	/**
	 * id
	 */
	private Long id;

	/**
	 * 用户本人id
	 */
	private Long userId;

	/**
	 * 好友id,也是用户id
	 */
	private Long friendId;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
