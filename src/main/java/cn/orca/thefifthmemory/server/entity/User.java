package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
public class User  implements Serializable {
	private static final long serialVersionUID =  4318251073598811350L;

	/**
	 * 用户id
	 */
	private Long id;

	/**
	 * 用户名（账号）,目前使用的是邮箱
	 */
	private String username;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 昵称
	 */
	private String nick;

	/**
	 * 性别
	 */
	private Integer gender;

	/**
	 * 头像图片路径
	 */
	private String avatar;

	/**
	 * 账户激活码
	 */
	private String activeCode;

	/**
	 * 账户状态，0=未激活,1=激活,2=锁定
	 */
	private Integer accountStatus;

	/**
	 * 个人签名
	 */
	private String signature;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
