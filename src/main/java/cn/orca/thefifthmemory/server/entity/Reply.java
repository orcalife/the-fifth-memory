package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Reply  implements Serializable {
	private static final long serialVersionUID =  1948429825948933902L;

	/**
	 * 回复表id
	 */
	private Long id;

	/**
	 * 回复的评论id
	 */
	private Long replyId;

	/**
	 * 本评论id
	 */
	private Long commentId;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
