package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Post  implements Serializable {
	private static final long serialVersionUID =  6115801201149809371L;

	/**
	 * 帖子id
	 */
	private Long id;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 帖子主人id
	 */
	private Long userId;

	/**
	 * 话题id
	 */
	private Long topicId;

	/**
	 * 点赞量
	 */
	private Long likeCount;

	/**
	 * 浏览量
	 */
	private Long viewCount;

	/**
	 * 评论量
	 */
	private Long commentCount;

	/**
	 * 帖子状态,0=审核通过发布,1=待审核，2=草稿，3=审核未通过
	 */
	private Integer postState;

	private Integer top;
	private Integer visible;
	private String label;
	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
