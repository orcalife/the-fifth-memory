package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Topic  implements Serializable {
	private static final long serialVersionUID =  7520818754937566243L;

	/**
	 * 话题id
	 */
	private Long id;

	/**
	 * 话题名称
	 */
	private String topicName;

	/**
	 * 话题图标
	 */
	private String topicImg;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

	/*
	* 话题状态，是否启用话题
	* */
	private Integer status;

}
