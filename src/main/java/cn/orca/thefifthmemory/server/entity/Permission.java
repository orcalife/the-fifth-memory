package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Permission  implements Serializable {
	private static final long serialVersionUID =  6411087042662791517L;

	/**
	 * 权限id
	 */
	private Long id;

	private String permissionName;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
