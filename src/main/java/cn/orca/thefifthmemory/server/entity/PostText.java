package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class PostText  implements Serializable {
	private static final long serialVersionUID =  5065585237912675121L;

	/**
	 * id
	 */
	private Long id;

	/**
	 * 帖子id
	 */
	private Long postId;

	/**
	 * 帖子文本内容
	 */
	private String postText;

}
