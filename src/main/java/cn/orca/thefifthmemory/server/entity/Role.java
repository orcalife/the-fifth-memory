package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Role  implements Serializable {
	private static final long serialVersionUID =  3571103876688958073L;

	/**
	 * 角色id
	 */
	private Long id;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
