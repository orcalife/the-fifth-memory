package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class PostImg  implements Serializable {
	private static final long serialVersionUID =  7450142737653624257L;

	/**
	 * 图片id
	 */
	private Long id;

	/**
	 * 帖子id
	 */
	private Long postId;

	/**
	 * 图片路径字符串
	 */
	private String imgUrl;

}
