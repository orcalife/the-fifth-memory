package cn.orca.thefifthmemory.server.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Comment  implements Serializable {
	private static final long serialVersionUID =  3469796091763266167L;

	/**
	 * 评论id
	 */
	private Long id;

	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 * 帖子id
	 */
	private Long postId;

	/**
	 * 评论内容
	 */
	private String commentText;

	/**
	 * 点赞量
	 */
	private Long likeCount;

	/**
	 * 回复量
	 */
	private Long replyCount;

	/**
	 * 创建时间
	 */
	private LocalDateTime gmtCreate;

	/**
	 * 最后修改时间
	 */
	private LocalDateTime gmtModified;

}
