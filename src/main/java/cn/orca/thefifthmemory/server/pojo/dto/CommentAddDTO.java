package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class CommentAddDTO implements Serializable {
    @NotNull(message = "错误操作")
    private Long postId;
    @NotNull(message = "评论不能为空")
    @NotBlank(message = "评论不能为空")
    private String  commentText;
}
