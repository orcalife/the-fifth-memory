package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@Data
public class TopicDTO implements Serializable {
    /**
     * 话题名称
     */
    private Long id;
    @NotNull(message = "话题名称不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9]+$",message = "话题名称只能是中文、英文、数字的组合")
    private String topicName;
    /**
     * 话题图标
     */
    @NotNull(message = "话题图标不能为空")
    private List<String> imgUrls;
    private String topicImg;
}
