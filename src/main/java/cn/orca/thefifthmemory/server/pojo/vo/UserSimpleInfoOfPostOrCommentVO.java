package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;
import java.io.Serializable;

@Data
public class UserSimpleInfoOfPostOrCommentVO implements Serializable {
    /** 用户id */
    private Long id ;
    /** 昵称 */
    private String nick ;
    /** 头像图片路径 */
    private String avatar ;
}
