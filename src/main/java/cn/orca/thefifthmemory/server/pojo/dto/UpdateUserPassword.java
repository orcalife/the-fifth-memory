package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;


@Data
public class UpdateUserPassword implements Serializable {
    @Pattern(regexp = "^[a-zA-Z]\\w{5,17}$",message = "密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)")
    private String oldPassword;
    @Pattern(regexp = "^[a-zA-Z]\\w{5,17}$",message = "密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)")
    private String newPassword;
    private String username;
}
