package cn.orca.thefifthmemory.server.pojo.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("管理员登录DTO")
@Data
public class AdminLoginDTO implements Serializable {

    @ApiModelProperty(value = "用户名",name = "username",example = "admin")
    @NotNull(message = "登录失败，请提交用户名！")
    private String username;

    @ApiModelProperty(value = "密码",name = "password",example = "abc123")
    @NotNull(message = "登录失败，请提交密码！")
    private String password;

}
