package cn.orca.thefifthmemory.server.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PostDetailVO {
    /**
     * 帖子id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 话题id
     */
    private Long topicId;

    /**
     * 点赞量
     */
    private Long likeCount;

    /**
     * 浏览量
     */
    private Long viewCount;

    /**
     * 评论量
     */
    private Long commentCount;

    /**
     * 帖子状态,0=草稿,1=待审核，2=审核通过发布，3=审核未通过
     */
    private Integer postState;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime gmtCreate;

    /**
     * 最后修改时间
     */
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime gmtModified;

    /**
     * 帖子文本
     */
    private String postText;

    /**
     * 用户
     */
    private UserSimpleInfoOfPostOrCommentVO user;

    /**
     * 帖子图片
     */



    private List<String> imgUrl;
    /*
    * 帖子所属话题
    * */
    private TopicSimpleInfoVO topic;



}
