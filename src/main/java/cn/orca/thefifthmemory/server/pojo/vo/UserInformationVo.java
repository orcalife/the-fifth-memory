package cn.orca.thefifthmemory.server.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserInformationVo implements Serializable {
    /** 用户id */
    private Long id ;
    /** 邮箱 */
    private String username;
    /** 昵称 */
    private String nick ;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate ;
    /** 性别 */
    private Integer gender ;
    /** 头像图片路径 */
    private String avatar ;
//    个人签名
    private String signature;
    // 用户状态
    private  Integer accountStatus;

}
