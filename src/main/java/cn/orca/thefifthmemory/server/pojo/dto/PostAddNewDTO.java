package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 增加一个帖子DTO类
 */

@Data
public class PostAddNewDTO {

    /**
     * 前端的传过来的 post
     *  post: {
     *         title: "",
     *         postText: "",
     *         topicId: "",
     *       },
     */


    /**
     * 帖子标题
     */
    @NotNull
    private String title;

    /**
     * 帖子文本内容
     */
    @NotNull(message = "日记内容不能为空")
    @NotBlank(message = "日记内容不能为空")
    private String postText;

    /**
     * 话题id
     */
    private Long topicId;

    private List<String> imgUrls;

    private Integer visible;

}
