package cn.orca.thefifthmemory.server.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommentVO {
    private Long postId;//帖子ID
    private Long id;//评论ID
    private Long userId;//用户ID
    private String commentText;//评论内容
    private Long likeCount;//点赞量
    private Long replyCount;//回复量
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime gmtCreate;
    private String nick;//昵称
    private String username;//用户名
    private String avatar;//头像

}

