package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserLoginDTO implements Serializable{
    @Pattern(regexp = "^[a-zA-Z]\\w{5,17}$",message = "密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)")
    private String password ;
    @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$",message = "账号(邮箱)错误,请检查是否正确")
    private String username;
}
