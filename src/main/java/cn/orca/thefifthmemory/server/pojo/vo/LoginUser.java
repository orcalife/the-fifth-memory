package cn.orca.thefifthmemory.server.pojo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class LoginUser implements UserDetails {

    private UserLoginVO userLoginVO;

    public LoginUser(UserLoginVO userLoginVO) {
        this.userLoginVO = userLoginVO;
    }

    /*不序列化*/
    @JSONField(serialize = false)
    private  List<SimpleGrantedAuthority> simpleGrantedAuthorityList;

    /*返回登录用户的id*/
    public Long getUserId(){
        return userLoginVO.getId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        /*List<SimpleGrantedAuthority> simpleGrantedAuthorityList = null;
        for (String p:userLoginVO.getPermissions()
             ) {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(p);
            simpleGrantedAuthorityList.add(authority);
        }
        */
        if (simpleGrantedAuthorityList != null) {
            return simpleGrantedAuthorityList;
        }
//        函数式编程
        simpleGrantedAuthorityList = userLoginVO.getPermissions().
                stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return simpleGrantedAuthorityList;
    }

    @Override
    public String getPassword() {
        return userLoginVO.getPassword();
    }

    @Override
    public String getUsername() {
        return userLoginVO.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
