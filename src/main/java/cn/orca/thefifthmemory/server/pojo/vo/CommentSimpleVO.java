package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CommentSimpleVO implements Serializable {
    private Long postId;
    private Long id;
    private Long userId;
    private String commentText;
    private Long likeCount;
    private Long replyCount;
    private LocalDateTime gmtCreate;
}
