package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserUploadInfoDTO implements Serializable {
    /** 昵称 */
    @NotNull(message = "昵称不能为空")
    @NotBlank(message = "昵称不能为空")
    private String nick ;
    /** 性别 */
    @NotNull(message = "性别不能为空")
    private Integer gender ;
    /*个人签名*/
    @NotNull(message = "签名不能为空")
    @NotBlank(message = "签名不能为空")
    private String signature;
    private String userName;


}
