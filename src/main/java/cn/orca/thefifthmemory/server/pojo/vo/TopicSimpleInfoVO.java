package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TopicSimpleInfoVO implements Serializable {
    /**
     * 话题id
     */
    private Long id;

    /**
     * 话题名称
     */
    private String topicName;

    /**
     * 话题图标
     */
    private String topicImg;
}
