package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CommentReplyAddDTO {
    @NotNull(message = "回复不能为空")
    private String  commentText;
    @NotNull(message = "你没有选择要回复的评论")
    private Long commentId;
}
