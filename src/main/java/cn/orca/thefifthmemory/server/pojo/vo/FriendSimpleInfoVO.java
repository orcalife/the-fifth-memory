package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FriendSimpleInfoVO implements Serializable {
    /** 用户id */
    private Long id ;
    /** 昵称 */
    private String nick ;
    /** 头像图片路径 */
    private String avatar ;
    /**
     * 个人签名
     */
    private String signature;
}
