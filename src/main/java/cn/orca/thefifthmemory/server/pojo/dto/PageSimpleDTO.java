package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageSimpleDTO implements Serializable {
    // 帖子页码 从1开始的
    private int pageNum;
    // 显示上限  每页最多有多少帖子
    private int pageSize;

    private int skipPage;
    public void setSkipPage(int pageNum, int pageSize) {
        this.skipPage = (pageNum - 1) * pageSize;
    }



}
