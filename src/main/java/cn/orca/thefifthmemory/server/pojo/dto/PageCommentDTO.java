package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageCommentDTO implements Serializable {

    private int id;
    private int pageNum;

    private int pageSize;

    private int skipPage;

    public void setSkipPage(int pageNum, int pageSize) {
        this.skipPage = (pageNum - 1) * pageSize;
    }


}
