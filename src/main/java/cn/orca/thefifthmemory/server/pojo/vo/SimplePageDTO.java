package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimplePageDTO implements Serializable {
    private Integer pageNum;
    private Integer pageSize;
}