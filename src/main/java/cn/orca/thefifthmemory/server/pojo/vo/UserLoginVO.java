package cn.orca.thefifthmemory.server.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserLoginVO implements Serializable{
    private Long id;
    private String username ;
    private String password ;
    private Integer accountStatus;
    private List<String> permissions;
}
