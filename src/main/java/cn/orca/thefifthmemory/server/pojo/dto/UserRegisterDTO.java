package cn.orca.thefifthmemory.server.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class UserRegisterDTO implements Serializable{
    @Pattern(regexp = "^[a-zA-Z]\\w{5,17}$",message = "密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)")
    private String password ;
    @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$",message = "邮箱错误,请检查是否正确")
    private String email ;
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$",message = "中文、英文、数字包括下划线")
    private String nick ;
    @NotNull
    private String activeCode ;

}
