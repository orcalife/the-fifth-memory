package cn.orca.thefifthmemory.server.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AllTopicVO implements Serializable {

	/**
	 * 话题id
	 */
	private Long id;

	/**
	 * 话题名称
	 */
	private String topicName;

	/**
	 * 话题图标
	 */
	private String topicImg;

	/**
	 * 话题下的帖子数量
	 */
	private Long postsNumber;
	/** 创建时间 */
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
	private Date gmtCreate ;
	/*
	 * 话题状态，是否启用话题
	 * */
	private Integer status;
}
