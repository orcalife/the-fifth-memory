package cn.orca.thefifthmemory.server.controller;

import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;
import cn.orca.thefifthmemory.server.service.IFriendService;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "FriendController", description = "好友相关接口")
@RestController
@RequestMapping("/friend")
public class FriendController {

    @Autowired
    private IFriendService friendService;

    @Operation(description = "首页好友列表")
    @GetMapping("/showFriends")
    public JsonResult<List<FriendSimpleInfoVO>> showFriends(){
        return JsonResult.ok(friendService.showFriends());
    }

    @Operation(description = "添加好友")
    @GetMapping("/addFriend/{friendId}")
    public JsonResult addFriend(@PathVariable Long friendId){
        friendService.addFriend(friendId);
        return JsonResult.ok();
    }

    @Operation(description = "删除好友")
    @GetMapping("/deleteFriend/{friendId}")
    public JsonResult deleteFriend(@PathVariable Long friendId){
        friendService.deleteFriend(friendId);
        return JsonResult.ok();
    }

    @Operation(description = "是否是好友")
    @GetMapping("/isFriend/{friendId}")
    public JsonResult<Boolean> isFriend(@PathVariable Long friendId){
        Boolean isFriend = friendService.isFriend(friendId);
        return JsonResult.ok(isFriend);
    }

}
