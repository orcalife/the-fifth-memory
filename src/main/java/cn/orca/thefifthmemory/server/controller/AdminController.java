package cn.orca.thefifthmemory.server.controller;

import cn.orca.thefifthmemory.server.pojo.dto.AdminLoginDTO;
import cn.orca.thefifthmemory.server.service.IAdminService;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/admin", produces = "application/json; charset=utf-8")
@Api(tags = "管理员模块")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    // 以下是测试访问的请求
    @GetMapping("/hello")
    @ApiOperation("测试用")
    @PreAuthorize("hasAuthority('/ams/admin/read')")
    public String sayHello() {
        return "hello~~~";
    }

    // http://localhost:8080/admins/login?username=root&password=123456
    @PostMapping("/login")
    @ApiOperation("登录")
    public JsonResult<String> login(@RequestBody @Validated AdminLoginDTO adminLoginDTO) {
        System.out.println("AdminController.login");
        System.out.println("======================================================AdminController.login");
        System.out.println("adminLoginDTO = " + adminLoginDTO);
        String jwt = adminService.login(adminLoginDTO);
        System.out.println("dskfjdskfjsdkolujfoiewjfdsokljfoisdjfoidsjf");
        return JsonResult.ok(jwt);
    }

    @ApiOperation("退出登录")
    @GetMapping("/logout")
    public JsonResult<Void> logout() {

        return JsonResult.ok();
    }


}
