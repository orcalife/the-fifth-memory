package cn.orca.thefifthmemory.server.controller;
import cn.orca.thefifthmemory.server.pojo.dto.CommentAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.CommentReplyAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PageCommentDTO;
import cn.orca.thefifthmemory.server.pojo.vo.CommentVO;
import cn.orca.thefifthmemory.server.service.ICommentService;
import cn.orca.thefifthmemory.server.util.Page;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@Tag(name = "CommentController",description = "评论模块")
@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private ICommentService service;
    @Operation(description = "添加评论")
    @PostMapping("/addComment")
    public JsonResult<Void> addComment(@RequestBody @Validated CommentAddDTO comment){
        log.info("控制层-添加评论方法执行");
        service.addComment(comment);
        return JsonResult.ok();
    }

    @Operation(description = "添加回复")
    @PostMapping("/addReply")
    public JsonResult<Void> addReply(@RequestBody @Validated CommentReplyAddDTO commentDTO){
        service.addReply(commentDTO);
        return JsonResult.ok();
    }
    @Operation(description = "展示帖子下的所有评论")
    @GetMapping ("/allComment/{postId}/{currentPage}")
    public JsonResult<List<CommentVO>> QueryAllComment(@PathVariable Long postId,@PathVariable int currentPage){
        List<CommentVO> commentVOList=service.listAllComments(postId,currentPage);
        return JsonResult.ok(commentVOList);
    }
    @Operation(description = "展示帖子下的所有评论")
    @PostMapping ("/allCommentPage")
    public JsonResult<List<CommentVO>> QueryAllCommentPage(@Validated @RequestBody PageCommentDTO page){
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        List<CommentVO> commentVOList=service.listAllCommentsPage(page);
        return JsonResult.ok(commentVOList);
    }
    @Operation(description = "展示评论下的直接回复")
    @GetMapping("/allReply")
    public JsonResult<List<CommentVO>> QueryAllReply(Long commentId){
        List<CommentVO> commentVOList=service.listReplyByCommentId(commentId);
        return JsonResult.ok(commentVOList);
    }
    @Operation(description = "删除对应评论及其回复")
    @GetMapping("/deleteComment/{id}")
    public JsonResult<Void> deleteComment(Long id){
        service.deleteComment(id);
        return JsonResult.ok();
    }
}
