package cn.orca.thefifthmemory.server.controller;

import cn.orca.thefifthmemory.server.pojo.dto.*;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.service.IUserService;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/user")
@Tag(name = "UserController", description = "用户相关接口")
public class UserController {
    @Autowired
    private IUserService userService;

    @Operation(description = "用户获取验证码")
    @GetMapping("/getActiveCode/{mail}")
    public JsonResult<Void> sendActiveCode(
            @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", message = "邮箱错误,请检查是否正确")
            @PathVariable String mail) {
        log.debug("控制层-通过邮箱获取验证码:{}", mail);
        userService.sendActiveCode(mail);
        return JsonResult.ok();
    }

    @Operation(description = "注册")
    @PostMapping("/reg")
    public JsonResult<Void> register(@RequestBody @Validated UserRegisterDTO userRegisterDTO) {
        log.debug("控制层-用户提交的注册信息:{}", userRegisterDTO);
        userService.register(userRegisterDTO);
        return JsonResult.ok();
    }

    @Operation(description = "登录")
    @PostMapping("/login")
    public JsonResult<String> login(@RequestBody @Validated UserLoginDTO userLoginDTO) {
        log.debug("控制层-用户提交的登录信息:{}", userLoginDTO);
        String jwt = userService.login(userLoginDTO);
        return JsonResult.ok(jwt, "登录成功");
    }

    @Operation(description = "获取当前登录用户个人信息")
    @GetMapping("/personalInfo")
    public JsonResult<UserInformationVo> personalInfo(HttpServletRequest request) {
        UserInformationVo userInformationVo = userService.getCurrentUserInfo(request);
        return JsonResult.ok(userInformationVo);
    }

    @Operation(description = "根据id获取用户个人信息")
    @GetMapping("/userInfoById/{id}")
    public JsonResult<UserInformationVo> userInfoById(@PathVariable Long id) {
        UserInformationVo userInformationVo = userService.userInfoById(id);
        return JsonResult.ok(userInformationVo);
    }


    @Operation(description = "修改个人信息")
    @PostMapping("/updatePersonalInfo")
    public JsonResult<Void> updatePersonalInfo(@RequestBody @Validated UserUploadInfoDTO userUploadInfoDTO) {
        userService.updatePersonalInfo(userUploadInfoDTO);
        return JsonResult.ok();
    }

    @Operation(description = "修改个人密码")
    @PostMapping("/updatePassword")
    public JsonResult<Void> updatePassword(@RequestBody @Validated UpdateUserPassword updateUserPassword) {
        userService.updatePassword(updateUserPassword);
        return JsonResult.ok();
    }

    /**
     * 接受用户上传的图片
     * 返回图片名字供用户后期改变
     *
     * @param picFile
     * @return
     */
    @Operation(description = "用户更换头像")
    @PostMapping("/upload")
    public JsonResult<String> uploadImg(MultipartFile picFile) {
        log.debug("用户控制层-uploadImg方法-picFile参数值：{}", picFile);
        String img = userService.uploadImg(picFile);
        return JsonResult.ok(img);
    }

    @Operation(description = "上传用户头像，保存在数据库中")
    @PostMapping("/saveChangeHeaderUrl")
    public JsonResult<Void> uploadSave(@RequestBody @NotNull(message = "请上传头像") List<String> imgUrls) {
        userService.uploadSave(imgUrls);
        log.debug("用户控制层-uploadSave方法执行");
        return JsonResult.ok();
    }

    @Operation(description = "退出登录")
    @GetMapping("/logout")
    public void logout() {
        // TODO 退出登录

    }

    @Operation(description = "忘记密码修改密码")
    @PostMapping("/forgetPassword")
    public JsonResult<Void> forgetPassword(@RequestBody @Validated ForgetPasswordDTO forgetPasswordDTO) {
        userService.forgetPassword(forgetPasswordDTO);
        return JsonResult.ok();
    }


    @Operation(description = "用户忘记密码获取验证码")
    @GetMapping("/getActiveCodeForgetPassword/{email}")
    public JsonResult<Void> sendActiveCodeForgetPassword(
            @Pattern(regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", message = "邮箱错误,请检查是否正确")
            @PathVariable String email) {
        log.debug("控制层-通过邮箱获取验证码:{}", email);
        userService.sendActiveCodeForgetPassword(email);
        return JsonResult.ok();
    }

    @Operation(description = "获取所有用户")
    @PostMapping("/allUser")
    public JsonResult<List<UserInformationVo>> getAllUser(@RequestBody @Validated PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(), page.getPageSize());
        log.debug("page = " + page);
        log.debug("调试：UserController-getAllUser:page:{}", page);
        List<UserInformationVo> users = userService.getAllUser(page);
        Long count = userService.countAllUser();
        return JsonResult.ok(users, count);
    }

    @Operation(description = "搜索用户")
    @GetMapping("/searchUser/{email}")
    public JsonResult<List<UserInformationVo>> searchUser(@Validated @Email(message = "邮箱格式错误,请检查是否正确")
                                                          @PathVariable String email) {
        log.debug("email = " + email);
        UserInformationVo user = userService.searchUser(email);
        ArrayList<UserInformationVo> userInformationVos = new ArrayList<>();
        userInformationVos.add(user);
        return JsonResult.ok(userInformationVos);
    }

    @PreAuthorize("hasAuthority('/user/setUserStatus')")
    @Operation(description = "设置用户状态")
    @GetMapping("/setUserStatus/{id}/{accountStatus}")
    public JsonResult<Void> setUserStatus(@PathVariable Long id, @PathVariable Integer accountStatus) {
        log.debug("id = " + id + ", accountStatus = " + accountStatus);
        userService.setUserStatus(id,accountStatus);
        return JsonResult.ok();
    }

}
