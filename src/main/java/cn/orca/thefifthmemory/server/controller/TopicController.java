package cn.orca.thefifthmemory.server.controller;

import cn.orca.thefifthmemory.server.entity.Topic;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.TopicDTO;
import cn.orca.thefifthmemory.server.pojo.vo.AllTopicVO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import cn.orca.thefifthmemory.server.service.ITopicService;
import cn.orca.thefifthmemory.server.util.Page;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Tag(name = "TopicController", description = "话题相关接口")
@RestController
@RequestMapping("/topic")
public class TopicController {

    @Autowired
    private ITopicService topicService;

    @PostMapping("/getAllTopicByPage")
    public JsonResult<List<AllTopicVO>> getAllTopicByPage(@RequestBody @Validated PageSimpleDTO page){
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("page = " + page);
        List<AllTopicVO> allTopic = topicService.getAllTopicByPage(page);
        Long count = topicService.countAllTopic();
        return JsonResult.ok(allTopic,count);
    }
    @GetMapping("/getAllTopic")
    public JsonResult<List<AllTopicVO>> getAllTopic(){
        List<AllTopicVO> allTopic = topicService.getAllTopic();
        Long count = topicService.countAllTopic();
        return JsonResult.ok(allTopic,count);
    }

    @GetMapping("/getAllPostByTopic/{topicId}")
    public JsonResult<List<PostDetailVO>> getAllPostByTopic(@PathVariable Long topicId){
        log.debug("调试TopicController-getAllPostByTopic-topicId值为{}",topicId);
        return JsonResult.ok(topicService.getAllPostByTopic(topicId));
    }

    @PreAuthorize("hasAuthority('/topic/updateTopic')")
    @PostMapping("/updateTopic")
    public JsonResult updateTopic(@RequestBody @Validated TopicDTO topicDTO){
        log.info("话题控制层-addNewTopic方法-topicDTO参数值：{}",topicDTO);
        topicService.updateTopic(topicDTO);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAuthority('/topic/addNewTopic')")
    @PostMapping("/addNewTopic")
    public JsonResult addNewTopic(@RequestBody @Validated TopicDTO topicDTO){
        log.info("话题控制层-addNewTopic方法-topicDTO参数值：{}",topicDTO);
        topicService.addNewTopic(topicDTO);
        return JsonResult.ok();
    }

    @Operation(description = "上传话题图标")
    @PostMapping("/upload")
    public JsonResult<String> uploadImg(MultipartFile picFile) {
        log.debug("话题控制层-uploadImg方法-picFile参数值：{}",picFile);
        String img = topicService.uploadImg(picFile);
        return JsonResult.ok(img);
    }


    @GetMapping("/getAllEnableTopic")
    public JsonResult<List<Topic>> getAllEnableTopic(){
        List<Topic> allUseTopic = topicService.getAllUseTopic();
        return JsonResult.ok(allUseTopic);
    }
    @GetMapping("/getAllDisableTopic")
    public JsonResult<List<Topic>> getAllDisableTopic(){
        List<Topic> allNoUseTopic = topicService.getAllNoUseTopic();
        return JsonResult.ok(allNoUseTopic);
    }
    @GetMapping("/updateStatusDisable/{id}")
    public JsonResult updateStatusDisable(@PathVariable Long id){
        topicService.updateTopiToDisable(id);
        return JsonResult.ok();
    }
    @GetMapping("/updateStatusEnable/{id}")
    public JsonResult updateStatusEnable(@PathVariable Long id){
        topicService.updateTopiToEnable(id);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAuthority('topic/setTopicStatus')")
    @Operation(description = "设置话题状态")
    @GetMapping("/setTopicStatus/{id}/{status}")
    public JsonResult<Void> setUserStatus(@PathVariable Long id, @PathVariable Integer status) {
        log.debug("id = " + id + ", accountStatus = " + status);
        topicService.setTopicStatus(id,status);
        return JsonResult.ok();
    }
}
