package cn.orca.thefifthmemory.server.controller;

import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PostAddNewDTO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import cn.orca.thefifthmemory.server.service.IPostService;
import cn.orca.thefifthmemory.server.web.JsonResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

@Tag(name = "postController", description = "帖子相关接口")
@Slf4j
@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    IPostService postService;

    @Operation(description = "用户写帖子日记-服务器插入帖子")
    @PostMapping("/write")
    public JsonResult<Void> insert(@Validated @RequestBody PostAddNewDTO postAddNewDTO) {
        log.debug("调试：PostController-insert：写日记请求上传参数-PostAddNewDTO值为：{}", postAddNewDTO);
        postService.insert(postAddNewDTO);
        return JsonResult.ok();
    }

    @PreAuthorize("hasAuthority('post/delete')")
    @Operation(description = "根据帖子id删除帖子")
    @PostMapping("/delete")
    public JsonResult<Void> delete(@RequestBody @NotNull(message = "请选择要看删除的帖子") List<Long> postIds) {
        log.debug("调试：PostController-delete：删除帖子id{}", postIds);
        postService.deleteById(postIds);
        return JsonResult.ok();
    }

    @Operation(description = "帖子详情页展示")
    @GetMapping("/showDetail/{postId}")
    public JsonResult<PostDetailVO> getDetail(@PathVariable Long postId) {
        log.debug("帖子详情页方法执行");
        log.debug("调试PostController-getDetail-postId:{}",postId);
        PostDetailVO postDetailVO = postService.getDetail(postId);
        return JsonResult.ok(postDetailVO);
    }

    @Operation(description = "首页最新帖子展示,返回值为帖子实体类数组")
    @GetMapping("/showByTime/{page}")
    public JsonResult<List<PostDetailVO>> getPostsByTime(@PathVariable Integer page) {
        log.info("调试：PostController-getPostsByTime方法执行");
        return JsonResult.ok(postService.getPostsByTime(page));
    }

    @Operation(description = "首页精选帖子展示,返回值为帖子实体类数组")
    @GetMapping("/showByLike/{page}")
    public JsonResult<List<PostDetailVO>> getPostsByLike(@PathVariable Integer page) {
        return JsonResult.ok(postService.getPostsByLike(page));
    }

    @Operation(description = "首页最新帖子展示,返回值为帖子实体类数组")
    @PostMapping("/showByTimePage")
    public JsonResult<List<PostDetailVO>> getPostsByTimePage(@RequestBody @Validated PageSimpleDTO page) {
        log.debug("调试：PostController-getPostsByTime方法执行");
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("PostServiceImpl.getPostsByTime");
        log.debug("page = " + page);
        return JsonResult.ok(postService.getPostsByTimePage(page),postService.getTotal());
    }

    @Operation(description = "首页精选帖子展示,返回值为帖子实体类数组")
    @PostMapping("/showByLikePage")
    public JsonResult<List<PostDetailVO>> getPostsByLikePage(@RequestBody @Validated PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("PostServiceImpl.getPostsByTime");
        log.debug("page = " + page);
        return JsonResult.ok(postService.getPostsByLikePage(page),postService.getTotal());
    }


    @Operation(description = "展示登录用户的所有帖子")
    @GetMapping("/getUserAllPost")
    public JsonResult<List<PostDetailVO>> getUserAllPost() {
        return JsonResult.ok(postService.getUserAllPost());
    }

    @Operation(description = "上传帖子图片，返回值说明，参数说明")
    @PostMapping("/upload")
    public JsonResult<String> uploadImg(MultipartFile picFile) {
        String imgUrl = postService.uploadImg(picFile);
        return JsonResult.ok(imgUrl);
    }

    @Operation(description = "上传帖子图片，返回值说明，参数说明")
    @GetMapping("/remove")
    public JsonResult<Void> removeImg(String name) {
        log.debug("调试：PostController-uploadImg方法执行-图片名：{}",name);
        postService.removeImg(name);
        return JsonResult.ok();
    }

    @Operation(description = "贴子点赞")
    @GetMapping("/like/{postId}")
    public JsonResult<Void> likeAdd(@PathVariable Long postId) {
        log.debug("调试：PostController-likeAdd方法执行");
        postService.likeAdd(postId);
        return JsonResult.ok();
    }


    /**
     * 获取帖子总页数
     */
    @GetMapping("/totalPost")
    public JsonResult<Long> getTotalPost() {
        Long total = postService.getTotal();
        log.debug("调试：PostController-getTotalPost获取帖子总条数值为：{}",total);
        return JsonResult.ok(total);
    }

    @Operation(description = "根据用户id展示该用户的所有帖子")
    @GetMapping("/getUserAllPostById/{id}")
    public JsonResult<List<PostDetailVO>> getUserAllPostById(@PathVariable Long id) {
        return JsonResult.ok(postService.getUserAllPostById(id));
    }

    /*@Operation(description = "分页获取最旧未审核帖子")
    @GetMapping("/getOldTimeNonCheckedPosts/{page}")
    public JsonResult<List<PostDetailVO>> getNonCheckedPostsByOldTime(@PathVariable Integer page) {
        List<PostDetailVO> nonCheckedPostsByOldTime = postService.getOldTimeNonCheckedPosts(page);
        log.debug("调试：PostController-getNonCheckedPostsByOldTime-获取未审核帖子:{}", nonCheckedPostsByOldTime);
        return JsonResult.ok(nonCheckedPostsByOldTime);
    }*/

    @PreAuthorize("hasAuthority('/post/check')")
    @Operation(description = "根据帖子id设置帖子审核是否通过")
    @PostMapping("/check")
    public JsonResult<Void> setPostState(@RequestBody @NotNull(message = "请选择要审核的帖子") List<Long> postIds) {
        log.debug("调试：PostController-setPostState-设置帖子状态postId：{}", postIds);
        postService.setPostState(postIds);
        return JsonResult.ok();
    }
    @Operation(description = "根据帖子id设置帖子审核是否通过")
    @PostMapping("/cancelCheck")
    public JsonResult<Void> cancelCheck(@RequestBody @NotNull(message = "请选择要审核的帖子") List<Long> postIds) {
        log.debug("调试：PostController-setPostState-设置帖子状态postId：{}", postIds);
        postService.cancelCheck(postIds);
        return JsonResult.ok();
    }


    @Operation(description = "分页获取最新未审核帖子")
    @GetMapping("/getNewTimeNonCheckedPosts")
    public JsonResult<List<PostDetailVO>> getNewTimeNonCheckedPosts(@Validated @RequestBody PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        List<PostDetailVO> lastestNonCheckedPosts = postService.getNewTimeNonCheckedPosts(page);
        log.debug("调试：PostController-getNonCheckedPostsByOldTime-获取未审核帖子:{}", lastestNonCheckedPosts);
        Long count = postService.countAllNonCheck();
        return JsonResult.ok(lastestNonCheckedPosts,count);
    }

    @Operation(description = "分页获取最旧未审核帖子")
    @PostMapping("/getNonCheckedPostsByOldTimeAndPageCount")
    public JsonResult<List<PostDetailVO>> getNonCheckedPostsByOldTimeAndPageCount(@RequestBody @Validated PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("page = " + page);
        log.debug("调试：PostController-getNonCheckedPostsByOldTimeAndPageCount:page:{}",page);
        List<PostDetailVO> posts = postService.getNonCheckedPostsByOldTimeAndPageCount(page);
        Long count = postService.countAllNonCheck();
        return JsonResult.ok(posts,count);
    }

    @Operation(description = "获取所有帖子")
    @PostMapping("/getAllPosts")
    public JsonResult<List<PostDetailVO>> getAllPosts(@RequestBody @Validated PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("page = " + page);
        log.debug("调试：PostController-getAllPosts:page:{}",page);
        List<PostDetailVO> posts = postService.getAllPosts(page);
        Long count = postService.countAllPosts();
        return JsonResult.ok(posts,count);
    }

    @Operation(description = "获取所有已审核帖子")
    @PostMapping("/getAllPostsChecked")
    public JsonResult<List<PostDetailVO>> getAllPostsChecked(@RequestBody @Validated PageSimpleDTO page) {
        page.setSkipPage(page.getPageNum(),page.getPageSize());
        log.debug("page = " + page);
        log.debug("调试：PostController-getAllPosts:page:{}",page);
        List<PostDetailVO> posts = postService.getAllPostsChecked(page);
        Long count = postService.countAllPostsChecked();
        return JsonResult.ok(posts,count);
    }

}
