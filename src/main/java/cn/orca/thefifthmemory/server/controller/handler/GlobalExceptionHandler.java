package cn.orca.thefifthmemory.server.controller.handler;

import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.web.JsonResult;
import cn.orca.thefifthmemory.server.web.State;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义的错误
     * @param ex
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    public JsonResult<Void> handleServiceException(ServiceException ex) {
        return JsonResult.fail(ex.getState(), ex.getMessage());
    }

    /**
     * 处理数据格式错误异常
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    public JsonResult<Void> handleBindException(BindException ex) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        StringBuilder stringBuilder = new StringBuilder();
        for (FieldError fieldError : fieldErrors) {
            stringBuilder.append("；");
            stringBuilder.append(fieldError.getDefaultMessage());
        }
        String message = stringBuilder.substring(1);
        return JsonResult.fail(State.ERR_BAD_REQUEST, message);
    }

    /**
     * 处理未知异常
     * @param ex
     * @return
     */
    /*@ExceptionHandler(Throwable.class)
    public JsonResult<Void> handleServiceException(Throwable ex) {
        return JsonResult.fail(State.ERR_SERVER, ex.getMessage());
    }*/

    /**
     * 处理运行时异常
     * @param ex
     * @return
     */
    /*@ExceptionHandler(RuntimeException.class)
    public JsonResult<Void> handleRuntimeException(RuntimeException ex) {
        return JsonResult.fail(State.ERR_SERVER, ex.getMessage());
    }*/

    /**
     * 处理空指针异常
     * @param ex
     * @return
     */
    /*@ExceptionHandler(NullPointerException.class)
    public JsonResult<Void> handleServiceException(NullPointerException ex) {
        return JsonResult.fail(State.ERR_SERVER_NULLPOINTEREXCEPTION, ex.getMessage());
    }*/

}
