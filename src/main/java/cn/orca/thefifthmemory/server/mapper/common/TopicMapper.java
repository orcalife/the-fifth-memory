package cn.orca.thefifthmemory.server.mapper.common;

import cn.orca.thefifthmemory.server.entity.Topic;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.TopicDTO;
import cn.orca.thefifthmemory.server.pojo.vo.AllTopicVO;
import cn.orca.thefifthmemory.server.pojo.vo.TopicSimpleInfoVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicMapper {

    /**
     * 返回所有话题
     *
     * @param page
     * @return
     */
    List<AllTopicVO> getAllTopicByPage(PageSimpleDTO page);

    List<AllTopicVO> getAllTopic();


    /**
     * 通过id删除话题
     *
     * @param id 话题id
     */
    void deleteTopicById(Integer id);

    Long countPostsByTopic(Long topicId);

    /**
     * 添加新的话题
     *
     * @param topic
     */
    void insertNewTopic(Topic topic);

    /**
     * 根据话题名称查找话题
     *
     * @param name 话题名称
     * @return
     */
    TopicDTO getByName(String name);

    /**
     * 查询所有启用话题
     *
     * @param
     */
    List<Topic> getAllUseTopic();

    /**
     * 查询所有禁用话题
     *
     * @param
     */
    List<Topic> getAllNoUseTopic();

    /**
     * 根据“话题”id修改“是否启用此话题”
     *
     * @param id     话题id
     * @param status 话题状态
     * @return 受影响行数
     */
    int updateStatusById(@Param("id") Long id, @Param("status") Integer status);

    TopicSimpleInfoVO getSimpleInfoById(Long topicId);

    int updateTopic(TopicDTO topicDTO);

    Long countAllTopic();

    void setTopicStatus(@Param("id") Long id, @Param("state") Integer state);
}
