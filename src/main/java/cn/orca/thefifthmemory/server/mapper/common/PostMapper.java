package cn.orca.thefifthmemory.server.mapper.common;

import cn.orca.thefifthmemory.server.entity.Post;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Repository
public interface PostMapper {

    /**
     * 插入帖子
     * @param post 帖子对象
     * @return 返回1代表插入成功，否则插入失败
     */
    int insert(Post post);

    /**
     * 根据帖子id删除帖子
     */
    int deleteById(Long id);

    /**
     * 根据时间发布的先后顺序，返回最新帖子
     */
    List<PostDetailVO> getPostsByTimePage(PageSimpleDTO page);

    /**
     * 返回精选帖子，根据点赞数
     */
    List<PostDetailVO> getPostsByLikePage(PageSimpleDTO page);

    /**
     * 根据时间发布的先后顺序，返回最新帖子
     */
    List<PostDetailVO> getPostsByTime(
            @Param("page") Integer page,
            @Param("count") Integer count
    );

    /**
     * 返回精选帖子，根据点赞数
     */
    List<PostDetailVO> getPostsByLike(
            @Param("page") Integer page,
            @Param("count") Integer count
    );
    /**
     * 根据帖子id，获取帖子的详细信息
     * @param postId
     * @return
     */
     PostDetailVO getDetail(Long postId);

    /**
     * 获取当前用户的所有帖子
     * @return
     */
    List<PostDetailVO> getUserAllPost(Long id);

    /**
     * 根据userID，查询到公开帖子
     * @param id
     * @return
     */
     List<PostDetailVO> getUserPublicPost(Long id);

    /**
     * 根据userID，查询到给公开及给好友看的帖子
     * @param id
     * @return
     */
    List<PostDetailVO> getUserPublicAndFriendPost(Long id);

    /**
     * 点赞
     * @param postId
     */
    void likeAdd(Long postId);

    /**
     * 浏览
     * @param postId
     */
    void viewAdd(Long postId);

    /**
     * 评论增加
     * @param postId
     */
    void commentAdd(Long postId);


    /**
     * 获取同一话题的帖子
     */
    List<PostDetailVO> getPostsByTopicId(Long topicId);

    Long countPostsByTopic(Long topicId);

    /**
     * 获取帖子总数
     */
    Long countAll();

    /**
     * 分页实现获取最旧未审核帖子
     */
    /**
     * 设置帖子的状态
     */
    void setPostState(@Param("postId") Long postId);

    /**
     * 取消审核
     * @param postId
     */
    void cancelCheck(@Param("postId") Long postId);

    /**
     * 分页实现获取最旧未审核帖子
     */
    List<PostDetailVO> getOldTimeNonCheckedPosts(PageSimpleDTO page);

/*    *//**
     * 分页实现获取最新未审核帖子
     */
    List<PostDetailVO> getNewTimeNonCheckedPosts(PageSimpleDTO page);

    Long countAllNonCheck();

    List<PostDetailVO> getAllPosts(PageSimpleDTO page);

    Long countAllPosts();

    List<PostDetailVO> getAllPostsChecked(PageSimpleDTO page);

    Long countAllPostsChecked();


}
