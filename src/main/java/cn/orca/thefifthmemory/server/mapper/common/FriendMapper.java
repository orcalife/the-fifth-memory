package cn.orca.thefifthmemory.server.mapper.common;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FriendMapper {
   /**
    * 查询当前用户的所有好友的id
    * @param userId
    * @return
    */
   List<Long> queryAllFriendId(@Param("userId") Long userId);

   /**
    * 添加好友，既关注
    * @param userId
    * @param friendId
    * @return
    */
   int addFriend(@Param("userId") Long userId,@Param("friendId") Long friendId);

   void deleteFriend(@Param("userId") Long userId,@Param("friendId") Long friendId);

   int isFriend(@Param("userId") Long userId,@Param("friendId") Long friendId);
}
