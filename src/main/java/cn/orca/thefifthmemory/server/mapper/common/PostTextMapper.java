package cn.orca.thefifthmemory.server.mapper.common;

import cn.orca.thefifthmemory.server.entity.PostText;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface PostTextMapper {
    /**
     * 插入帖子文本
     */
    int insert(PostText postText);

    /**
     * 根据帖子id删除帖子文本
     */
    int deleteByPostId(Long id);


    /**
     * 根据帖子id获取帖子文本
     */
    String getPostTextByPostId(Long postId);
}
