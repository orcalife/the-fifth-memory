package cn.orca.thefifthmemory.server.mapper.common;

import cn.orca.thefifthmemory.server.entity.Comment;
import cn.orca.thefifthmemory.server.pojo.dto.PageCommentDTO;
import cn.orca.thefifthmemory.server.pojo.vo.CommentReplyVO;
import cn.orca.thefifthmemory.server.pojo.vo.CommentSimpleVO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentMapper {

    //插入评论
    int insertComment(Comment comment);
    //更新点赞量
    int updateLikeCount(Long id);

    //更新评论量
    int updateReplyCount(Long id);
    //查询帖子Id的所有评论
    List<CommentSimpleVO> listAllComments(Long postId);
    //根据评论Id查询所有回复评论
    List<CommentReplyVO> listReplyByCommentId(Long id);
    //删除评论
    int deleteCommentInComment(Long id);

    /*分页查询*/
    List<CommentSimpleVO> selectLimitComment(@Param("skipPage") int skipPage, @Param("pageSize") int pageSize, @Param("postId") Long postId);


    List<CommentSimpleVO> selectLimitCommentPage(PageCommentDTO page);
}
