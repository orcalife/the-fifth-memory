package cn.orca.thefifthmemory.server.mapper.common;
import cn.orca.thefifthmemory.server.entity.User;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.UpdateUserPassword;
import cn.orca.thefifthmemory.server.pojo.dto.UserUploadInfoDTO;
import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.pojo.vo.UserLoginVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserSimpleInfoOfPostOrCommentVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserMapper{

    /**
     * 查询邮箱
     * @param email
     * @return
     */
    String queryEmail(String email);

    /**
     * 插入用户
     * @param user
     * @return
     */
    int insert(User user);

    /**
     * 用户登录时，通过用户名查询用户名和密码，供security认证使用
     * @param username
     * @return
     */
    UserLoginVO queryUsername(String username);

    Integer queryUserAccountStatus(String username);


    /**
     * @param username 获取当前用户信息
     * @return
     */
    UserInformationVo queryUserInfoByUsername(String username);

    /**
     * 帖子或评论模块通过用户id查询到用户的简单信息(包括昵称，头像路径，id)
     * @param userid
     * @return
     */
    UserSimpleInfoOfPostOrCommentVO queryUserSimpleInfoById(Long userid);

    /**
     * 根据好友id查询好友的基本信息
     * @param friendId
     * @return
     */
    FriendSimpleInfoVO queryFreindSimpleInfoById(Long friendId);

    UserInformationVo userInfoById(Long id);

    /**
     *更新用户头像
     * @param imgUrl
     * @param userName
     * @return
     */
    int updateAvatar(@Param("imgUrl") String imgUrl, @Param("userName") String userName);

    int updateUserInfo(UserUploadInfoDTO userUploadInfoDTO);

    int updatePassword(UpdateUserPassword updateUserPassword);

    int forgetPassword(@Param("username") String username,@Param("password") String password);

    String queryPassword(String username);

    List<UserInformationVo> getAllUser(PageSimpleDTO page);

    Long countAllUser();

    UserInformationVo searchUser(String email);

    void setUserStatus(@Param("id") Long id, @Param("state") Integer state);


}
