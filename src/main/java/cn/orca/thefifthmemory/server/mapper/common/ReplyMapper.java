package cn.orca.thefifthmemory.server.mapper.common;

import cn.orca.thefifthmemory.server.entity.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplyMapper {
    /*增加回复*/
    int addReply(Reply reply);
    /*删除评论下的回复*/
    int deleteReplyById(Long commentId);
}
