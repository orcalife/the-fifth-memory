package cn.orca.thefifthmemory.server.mapper.common;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImgMapper {
    int insert(@Param("postId") Long postId, @Param("list") List<String> post_img_urls);
    List<String> queryImgsUrlByPostsId(Long postId);

    int delete(Long postId);
}
