package cn.orca.thefifthmemory.server.security;

import cn.orca.thefifthmemory.server.web.JsonResult;
import cn.orca.thefifthmemory.server.web.State;
import com.alibaba.fastjson.JSON;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证失败处理器：如用户名或密码不正确
 *
 * @author lxr
 */
@Component
public class AuthenticationFailHandlerImpl implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(JSON.toJSONString(JsonResult.fail(State.USERNAME_OR_PASSWORD_ERROR,"用户名或密码错误")));
    }
}
