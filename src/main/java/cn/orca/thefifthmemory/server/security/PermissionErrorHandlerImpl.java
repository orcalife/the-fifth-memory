package cn.orca.thefifthmemory.server.security;

import cn.orca.thefifthmemory.server.web.JsonResult;
import cn.orca.thefifthmemory.server.web.State;
import com.alibaba.fastjson.JSON;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证失败处理器：如用户名或密码不正确
 *
 * @author lxr
 */
@Component
public class PermissionErrorHandlerImpl implements AccessDeniedHandler {


    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
            httpServletResponse.setContentType("application/json;charset=UTF-8");
            httpServletResponse.getWriter().write(JSON.toJSONString(JsonResult.fail(State.ERR_PERMISSION_ERROR,"你没有这个权限哦！")));
        }

}
