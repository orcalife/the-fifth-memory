package cn.orca.thefifthmemory.server.security;

public interface JwtConst {
    String KEY_USERNAME = "username";
    String KEY_PERMISSIONS = "permissions";
}
