package cn.orca.thefifthmemory.server.security;

import cn.orca.thefifthmemory.server.mapper.admin.AdminMapper;
import cn.orca.thefifthmemory.server.mapper.common.UserMapper;
import cn.orca.thefifthmemory.server.pojo.vo.AdminLoginVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@Slf4j
public class AdminDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String path = request.getRequestURI();
        log.debug("请求路径:{}",path);
        UserLoginVO loginVO=new UserLoginVO();
        if ("/user/login".equals(path)){
            log.debug("根据用户名查询尝试登录的用户信息，用户名=" + s);
             loginVO = userMapper.queryUsername(s);
            log.debug("通过持久层进行查询，结果=" + loginVO);

            if (loginVO == null) {
                log.debug("根据用户名没有查询到有效的用户数据，将抛出异常");
                throw new BadCredentialsException("用户名或密码不正确");
            }

            log.debug("查询到匹配的管理员数据，需要将此数据转换为UserDetails并返回");
        }if ("/admin/login".equals(path)){
            AdminLoginVO adminLoginVO = adminMapper.getLoginInfoByUsername(s);
            log.debug("根据用户名查询尝试登录的管理员信息，用户名=" + s);
            try {
                BeanUtils.copyProperties(adminLoginVO,loginVO);
            } catch (Exception e) {
                e.printStackTrace();
            }
            log.debug("UserDetailsServiceImpl类-loadUserByUsername方法-userLoginVO值为：{}", loginVO);
            if (loginVO == null) {
                log.info("根据用户名没有查询到有效的管理员数据，将抛出异常");
                throw new BadCredentialsException("用户名或密码不正确！");
            }
        }


        UserDetails userDetails = User.builder()
                .username(loginVO.getUsername())
                .password(loginVO.getPassword())
                .accountExpired(false)
                .accountLocked(false)
                .disabled(false)
                .credentialsExpired(false)
                .authorities(loginVO.getPermissions().toArray(new String[] {}))
                .build();
        log.debug("转换得到UserDetails=" + userDetails);
        return userDetails;
    }

}
