package cn.orca.thefifthmemory.server.repository;

import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;

public interface UserRedisRepository {
    String USER_KEY = "user_ID:";
    /**
     * 判断是否存在id对应的缓存数据
     *
     * @param id 用户id
     * @return 存在则返回true，否则返回false
     */
    Boolean exists(Long id);

    /**
     * 向缓存中写入某id对应的空数据（null），此方法主要用于解决缓存穿透问题
     *
     * @param id 用户id
     */
    void saveEmptyValue(Long id);

    /**
     * 将用户详情存入到Redis中
     *
     * @param user 用户详细信息
     */
    void save(UserInformationVo user);

    /**
     * 根据id获取用户详情
     *
     * @param
     * @return 匹配的类别详情，如果没有匹配的数据，则返回null
     */
    UserInformationVo getDetailsByUsername(String username);
}
