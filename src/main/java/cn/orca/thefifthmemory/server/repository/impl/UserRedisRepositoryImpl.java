package cn.orca.thefifthmemory.server.repository.impl;

import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.pojo.vo.UserLoginVO;
import cn.orca.thefifthmemory.server.repository.UserRedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

@Repository
public class UserRedisRepositoryImpl implements UserRedisRepository {
    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;


    @Override
    public Boolean exists(Long id) {
        String key = USER_KEY + id;
        return redisTemplate.hasKey(key);
    }

    @Override
    public void saveEmptyValue(Long id) {
        String key = USER_KEY + id;
        redisTemplate.opsForValue().set(key, null, 30, TimeUnit.MINUTES);
    }

    @Override
    public void save(UserInformationVo user) {
        String key = USER_KEY + user.getUsername();
        redisTemplate.opsForValue().set(key, user);
    }

    @Override
    public UserInformationVo getDetailsByUsername(String username){
        String key = USER_KEY + username;
        Serializable result = redisTemplate.opsForValue().get(key);
        if (result == null) {
            return null;
        } else {
            UserInformationVo user = (UserInformationVo) result;
            return user;
        }
    }
}
