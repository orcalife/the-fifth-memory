package cn.orca.thefifthmemory.server.util;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 页面
 */
@Component
@Data
public class Page implements Serializable {

    // 当前页码  帖子页码 从1开始的
    private int currentPage;
    // 显示上限  每页最多有多少帖子
    private int pageSize;
    // 数据总数(用于计算总页数)  该页帖子总量
    private int rows;


/*
    public void setCurrentPage(Integer currentPage) {
        // 一开始没点击页码，传过来的是null
        if (currentPage == null) {
            this.currentPage = 1;
            return;
        }
        if (currentPage >= 1)
            this.currentPage = currentPage;
    }

    public void setPageSize(int pageSize) {

        this.pageSize = pageSize;
    }*/

/*
    public int getPageSize() {
        return pageSize;
    }*/


    public int getSkipPage() {
        return (currentPage - 1) * pageSize;
    }

    /**
     * 总页数
     *
     * @return
     */
    public int getPageSum() {
        int pageSum;
        return pageSum = rows % pageSize == 0 ? rows / pageSize : rows / pageSize + 1;
    }


}
