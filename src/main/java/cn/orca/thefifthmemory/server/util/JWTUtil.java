package cn.orca.thefifthmemory.server.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 该类的作用：根据用户id生成JWT，解析JWT拿到用户id，用该id去redis中查询到用户的基本信息及相关权限为后面的操作做准备
 * JWT：将和用户相关的一些信息，有效时间，等经过算法加密传输给用户
 * 解析JWT就是从JWT中解密出用户的信息
 */
@Slf4j
@Component
public class JWTUtil {
//    private static String secretKey= "klufgdvkijo453r8ds9oidsfdsfdu4@^yo$oiKNVFCDLA^*&(*(DKSFUJDS,NFVDASLF438594U3JREVnfdxfndhsa";
    private static String secretKey;

    @Value("thefifthmemory.jwt.secret-key")
    public  void setSecretKey(String key){
        secretKey=key;
        System.out.println(secretKey);
    }

    public String getSecretKey(){
        return secretKey;
    }

    /**
     * 生成JWT
     * TODO 将用户id放入
     */
    public static String GenerateJwt(Map<String, Object> claims){
        // Claims--存储用户相关信息，解析JWT就是从JWT中取出这部分信息
        // JWT的组成部分：Header（头），Payload（载荷），Signature（签名）
        String jwt = Jwts.builder()
                // Header：指定算法与当前数据类型
                // 格式为： { "alg": 算法, "typ": "jwt" }
                .setHeaderParam(Header.CONTENT_TYPE, "HS256")
                .setHeaderParam(Header.TYPE, Header.JWT_TYPE)
                // Payload：通常包含Claims（自定义数据）和过期时间，一小时
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + 1 * 60 * 60 * 1000))
                // Signature：由算法和密钥（secret key）这2部分组成
                .signWith(SignatureAlgorithm.HS256, secretKey)
                // 打包生成
                .compact();
        if (jwt == null) {

        }
        return  jwt;
    }

    /**
     * 解析JWT
     * @param jwt
     *
     */
    public static Claims ParseJwt(String jwt){
        if (jwt == null) {
            return null;
        }
        Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwt).getBody();
        return claims;
    }

    public static String getUserName(HttpServletRequest request){
        log.info("JWTUtil-getUserName方法执行");
        String jwt = request.getHeader("Authorization");
        Claims claims = ParseJwt(jwt);
        if (claims == null) {
            return null;
        }
        String username = (String) claims.get("username");
        log.debug("JWTUtil-getUserName-username值为：{}",username);
        return username;
    }


}
