package cn.orca.thefifthmemory.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

/**
 * 静态资源处理类
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File dir = new File("D:/Life-Images/");
        File dirHeader = new File("D:/Life-Images/Header-Images/");
        File dirTopic = new File("D:/Life-Images/Topic-Images/");
        File dirPost = new File("D:/Life-Images/Post-Images/");
        if (!dir.exists()){
            dir.mkdirs();
        }
        /*
        * 前端访问路径映射到对应的文件路径
        * /img/Header-Images/default.jpg
        * 对应：D:/Life-Images/Header-Images/default.jpg
        * */
        registry.addResourceHandler("/img/**").addResourceLocations("file:D:/Life-Images/");
    }
}

