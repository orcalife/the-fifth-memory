package cn.orca.thefifthmemory.server.config;

import cn.orca.thefifthmemory.server.security.AuthenticationFailHandlerImpl;
import cn.orca.thefifthmemory.server.security.JwtAuthenticationFilter;
import cn.orca.thefifthmemory.server.security.PermissionErrorHandlerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    /**
     * 注入该Bean以后，该Bean会自动调用方法匹配原密码与数据库中的密码
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 禁用防跨域攻击
        http.csrf().disable();

//        // 配置认证
//        http.formLogin()
//                // 哪个URL为登录页面
//                .loginPage("/admin/login");   //用户其他的页面没有认证，那么就转到这个登录页面

        // URL白名单
        String[] urls = {
//                用户模块白名单
                "/user/login",
                "/user/reg",
                "/user/getActiveCode/*",
                "/user/userInfoById/*",
                "/user/upload",
                "/user/forgetPassword",
                "/user/getActiveCodeForgetPassword/*",
//                 帖子模块白名单
                "/post/showDetail/*",
                "/post/showByTime/*",
                "/post/showByLike/*",
                "/post/showByTimePage",
                "/post/showByLikePage",
                "/post/totalPost",
                "/post/upload",
                "/post/getUserAllPostById/*",
//                话题模块白名单
                "/topic/getAllTopic/**",
                "/topic/getAllPostByTopic/**",
                "/topic/upload",
//                评论模块白名单
                "/comment/allComment/**",
                "/comment/allCommentPage/**",
                "/comment/allReply",
//                在线API白名单
                "/api-docs",
                "/swagger-ui/**",
                "/swagger-config",
                "/swagger-resources",
                "/v3/**",
//                静态文件白名单
                "/**/*.js",
                "/img/**",
                "/**/*.css",
                "/favicon.ico",
//                后台模块白名单
                "/admin/login",
                "/doc.html",
                "/v2/api-docs",
        };
        // 配置各请求路径的认证与授权
        http.authorizeRequests() // 请求需要授权才可以访问
                .antMatchers(urls) // 匹配一些路径
                .permitAll() // 允许直接访问（不需要经过认证和授权）
                .anyRequest() // 匹配除了以上配置的其它请求
                .authenticated(); // 都需要认证

        // 注册处理JWT的过滤器
        // 此过滤器必须在Spring Security处理登录的过滤器之前
        // request ---> Filter ---> DispatcherServlet ---> Interceptor ---> Controller
        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

        //配置异常处理器
        http.exceptionHandling()
                //配置认证失败处理器
                .authenticationEntryPoint(new AuthenticationFailHandlerImpl())
                .accessDeniedHandler(new PermissionErrorHandlerImpl())
        ;
    }

}
