package cn.orca.thefifthmemory.server.service;

import cn.orca.thefifthmemory.server.entity.Topic;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.TopicDTO;
import cn.orca.thefifthmemory.server.pojo.vo.AllTopicVO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ITopicService {

    String uploadImg(MultipartFile picFile);

    int removeImg(String name);
    /**
     * 返回所有话题
     * @return
     * @param page
     */
    List<AllTopicVO> getAllTopicByPage(PageSimpleDTO page);
    List<AllTopicVO> getAllTopic();

    List<PostDetailVO> getAllPostByTopic(Long topicId);

    /**
     * 增加新的话题
     * @param topicDTO
     */
    void addNewTopic(TopicDTO topicDTO);
    /**
     * 返回所有启用的话题
     * @return
     */
    List<Topic> getAllUseTopic();

    /**
     * 返回所有禁用话题
     * @return
     */
    List<Topic> getAllNoUseTopic();

    /**
     * 将禁用话题改为启用
     * @return
     */
    int updateTopiToEnable(Long id);

    /**
     * 将启用用话题改为禁用
     * @return
     */
    int updateTopiToDisable(Long id);


    void updateTopic(TopicDTO topicDTO);

    Long countAllTopic();

    void setTopicStatus(Long id, Integer status);

}
