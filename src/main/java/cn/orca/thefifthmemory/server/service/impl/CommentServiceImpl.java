package cn.orca.thefifthmemory.server.service.impl;

import cn.orca.thefifthmemory.server.entity.Comment;
import cn.orca.thefifthmemory.server.entity.Reply;
import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.mapper.common.CommentMapper;
import cn.orca.thefifthmemory.server.mapper.common.ReplyMapper;
import cn.orca.thefifthmemory.server.mapper.common.UserMapper;
import cn.orca.thefifthmemory.server.pojo.dto.CommentAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.CommentReplyAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PageCommentDTO;
import cn.orca.thefifthmemory.server.pojo.vo.*;
import cn.orca.thefifthmemory.server.service.ICommentService;
import cn.orca.thefifthmemory.server.service.IPostService;
import cn.orca.thefifthmemory.server.service.IUserService;
import cn.orca.thefifthmemory.server.util.Page;
import cn.orca.thefifthmemory.server.web.State;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class CommentServiceImpl implements ICommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ReplyMapper replyMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IPostService postService;
    @Autowired
    private IUserService service;
    @Autowired
    private IUserService userService;
    @Autowired
    private HttpServletRequest request;
    private UserInformationVo currentUserInfo;
    private Page page=new Page();
    /**
     * 获取当前用户信息
     */
    public void getCurrentUser(){
        currentUserInfo = userService.getCurrentUserInfo(request);
    }

    @Override
    public void addComment(CommentAddDTO commentDTO) {
        log.debug("评论:" + commentDTO);
        //TODO 评论逻辑实现
        Comment comment = new Comment();
        // 获取用户id
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long userId = currentUserInfo.getId();
        log.debug("CommentServiceImpl-addComment-当前用户id：{}",userId);
        log.info("为comment赋值");
        comment.setPostId(commentDTO.getPostId());
        comment.setUserId(userId);
        comment.setCommentText(commentDTO.getCommentText());
        LocalDateTime now = LocalDateTime.now();
        comment.setGmtCreate(now);
        comment.setGmtModified(now);
        comment.setLikeCount(0L);
        comment.setReplyCount(0L);
        log.info("执行添加评论");
        int rows = commentMapper.insertComment(comment);
        // 判断以上返回的结果是否不为1，抛出ServiceException异常
        if (rows != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "评论添加错误");
        }
        postService.commentAdd(commentDTO.getPostId());
        log.info("执行结束");
    }


    @Override
    public void addReply(CommentReplyAddDTO commentReplyAddDTO) {
        log.info("回复:" + commentReplyAddDTO);
        //TODO 回复逻辑实现
        //为创建的comment赋值
        UserInformationVo userInformationVo=service.getCurrentUserInfo(request);
        Long userId=userInformationVo.getId();
        Comment comment = new Comment();
        Reply reply = new Reply();
        comment.setUserId(userId);
        comment.setCommentText(commentReplyAddDTO.getCommentText());
        LocalDateTime now = LocalDateTime.now();
        comment.setGmtCreate(now);
        comment.setGmtModified(now);
        comment.setLikeCount(0L);
        comment.setReplyCount(0L);
//插入评论comment接收返回的ID

        log.info("执行插入评论");
        int rows = commentMapper.insertComment(comment);
//为reply设置回复Id,即当前comment的id

        reply.setReplyId(comment.getId());
//设置被回复即评论id,从commentReplyAddDTO中获取

        reply.setCommentId(commentReplyAddDTO.getCommentId());
        reply.setGmtCreate(now);
        reply.setGmtModified(now);
        // 判断以上返回的结果是否不为1，抛出ServiceException异常
        if (rows != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "评论添加错误");
        }

        log.info("执行插入回复");
        int rows1 = replyMapper.addReply(reply);
        if (rows1 != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "评论添加错误");
        }
        log.info("执行结束");
    }

//根据评论id删除评论

    @Override
    public void deleteComment(Long commentId) {
        log.info("删除ID({})", commentId);
        int rows1 = commentMapper.deleteCommentInComment(commentId);
        int rows2 = replyMapper.deleteReplyById(commentId);
        log.info("执行结束");
    }
    //显示帖子下的所有直接评论
    @Override
    public List<CommentVO> listAllComments(Long postId, int currentPage) {
        page.setCurrentPage(currentPage);
        log.info("CommentServiceImpl/listAllComments/postId:{},page:{}",postId,page);
        log.info("第几页:{}",page);
        List<CommentVO> commentVOList = new ArrayList<>();
        List<CommentSimpleVO> commentSimpleVOS = new ArrayList<>();
        log.info("数据库中查询数据");
        commentSimpleVOS = commentMapper.selectLimitComment(page.getSkipPage(),page.getPageSize(),postId);
        if (commentSimpleVOS == null) {
            /*数据库中确实没有则写入错误数据抛出异常*/
            throw new ServiceException(State.ERR_COMMENT_NOT_FOUND, "评论不存在!");
        }
        for (CommentSimpleVO simpleVO : commentSimpleVOS) {
            CommentVO commentVO = new CommentVO();
            BeanUtils.copyProperties(simpleVO, commentVO);
            commentVOList.add(commentVO);
        }
        setCompleteReply(commentVOList);
        return commentVOList;
    }
    //显示帖子下的所有直接评论
    @Override
    public List<CommentVO> listAllCommentsPage(PageCommentDTO page) {
        log.info("CommentServiceImpl/listAllComments/postId:{},page:{}",page.getId(),page);
        List<CommentVO> commentVOList = new ArrayList<>();
        List<CommentSimpleVO> commentSimpleVOS = new ArrayList<>();
        log.info("数据库中查询数据");
        commentSimpleVOS = commentMapper.selectLimitCommentPage(page);
        if (commentSimpleVOS == null) {
            /*数据库中确实没有则写入错误数据抛出异常*/
            throw new ServiceException(State.ERR_COMMENT_NOT_FOUND, "评论不存在!");
        }
        for (CommentSimpleVO simpleVO : commentSimpleVOS) {
            CommentVO commentVO = new CommentVO();
            BeanUtils.copyProperties(simpleVO, commentVO);
            commentVOList.add(commentVO);
        }
        setCompleteReply(commentVOList);
        return commentVOList;
    }
//展示评论下的所有直接回复
@Override
public List<CommentVO> listReplyByCommentId(Long commentId) {
    log.info("CommentServiceImpl/listReplyByCommentId/评论ID:{}",commentId);
    List<CommentVO> commentVOList = new ArrayList<>();
    List<CommentReplyVO> replyVOS = new ArrayList<>();

    log.info("数据库中查询数据");
    replyVOS = commentMapper.listReplyByCommentId(commentId);
    if (replyVOS== null) {
        throw new ServiceException(State.ERR_COMMENT_NOT_FOUND, "回复不存在!");
    }
    for (CommentReplyVO replyVO : replyVOS) {
        CommentVO commentVO = new CommentVO();
        BeanUtils.copyProperties(replyVO, commentVO);
        commentVOList.add(commentVO);
    }
    log.info("完善返回类设置用户和点赞属性等等");
    setCompleteReply(commentVOList);
    return commentVOList;
}

    public void setCompleteComment(List<CommentVO> list) {
        for (CommentVO commentVO : list) {
            //根据查询的评论中用户ID查到用户的头像昵称等信息放到要返回到客户端的评论VO里

            UserSimpleInfoOfPostOrCommentVO user = userMapper.queryUserSimpleInfoById(commentVO.getUserId());
            if (user==null)
            {
                throw new ServiceException(State.ERR_ACCOUNT_NOT_EXITS,"用户找不到！");
            }
            commentVO.setNick(user.getNick());
            commentVO.setAvatar(user.getAvatar());
        }
    }

    public void setCompleteReply(List<CommentVO> list) {
        for (CommentVO commentVO : list) {
            UserSimpleInfoOfPostOrCommentVO user = userMapper.queryUserSimpleInfoById(commentVO.getUserId());
            if (user==null)
            {
                throw new ServiceException(State.ERR_ACCOUNT_NOT_EXITS,"用户找不到！");
            }
            commentVO.setNick(user.getNick());
            commentVO.setAvatar(user.getAvatar());

        }
    }

}
