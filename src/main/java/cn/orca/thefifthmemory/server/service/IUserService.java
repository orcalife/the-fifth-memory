package cn.orca.thefifthmemory.server.service;

import cn.orca.thefifthmemory.server.pojo.dto.*;
import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.pojo.vo.UserSimpleInfoOfPostOrCommentVO;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface IUserService {

    /**
     * 用户获取验证码
     * @param mail
     */
    void sendActiveCode(String mail);

    /**
     * 用户注册
     * @param userRegisterDTO
     */
    void register(UserRegisterDTO userRegisterDTO);

    FriendSimpleInfoVO queryFriendInfo(Long friendId);


    /**
     * 用户登录
     * @param userLoginDTO
     */
    String login(UserLoginDTO userLoginDTO);


    /**
     * 获取当前用户信息
     * @return
     */
    UserInformationVo getCurrentUserInfo(HttpServletRequest request);


    /**
     * 帖子详情页作者信息，
     * 评论服务层传入作者id调用
     * 帖子服务层传入作者id调用
     * @param userid
     * @return
     */
    UserSimpleInfoOfPostOrCommentVO getUserSimpleInfo(Long userid);

    /**
     * 用户更新个人信息
     * @param userUploadInfoDTO
     */
    void updatePersonalInfo(UserUploadInfoDTO userUploadInfoDTO);

    /**
     * 用户更新头像
     * @param picFile
     * @return
     */
    String uploadImg(MultipartFile picFile);

    /**
     * 保存用户更新的头像
     */
    void uploadSave(List<String> imgUrls);

    /**
     * 缓存预热
     */
    void preloadCache();

    /*
    * 忘记密码功能
    * */

    /*
    * 修改密码功能
    * */
    void updatePassword( UpdateUserPassword updateUserPassword);

    void logout();

    UserInformationVo userInfoById(Long id);

    void sendActiveCodeForgetPassword(String email);
    void forgetPassword(ForgetPasswordDTO forgetPasswordDTO);

    List<UserInformationVo> getAllUser(PageSimpleDTO page);

    Long countAllUser();

    UserInformationVo searchUser(String email);

    void setUserStatus(Long id, Integer accountStatus);

}
