package cn.orca.thefifthmemory.server.service.impl;

import cn.orca.thefifthmemory.server.entity.Post;
import cn.orca.thefifthmemory.server.entity.PostText;
import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.mapper.common.*;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PostAddNewDTO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.service.IFriendService;
import cn.orca.thefifthmemory.server.service.IPostService;
import cn.orca.thefifthmemory.server.service.IUserService;
import cn.orca.thefifthmemory.server.util.Page;
import cn.orca.thefifthmemory.server.util.Upload;
import cn.orca.thefifthmemory.server.web.State;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class PostServiceImpl implements IPostService {
    private String dirPath = "D:/Life-Images/Post-Images/";

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private PostTextMapper postTextMapper;

    @Autowired
    private TopicMapper topicMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ImgMapper imgMapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private IUserService userService;
    private UserInformationVo currentUserInfo;
    @Autowired
    private IFriendService friendService;
    /**
     * 获取当前用户信息
     */
    public void getCurrentUser(){
        currentUserInfo = userService.getCurrentUserInfo(request);
    }

    /**
     * 插入帖子
     * @param postAddNewDTO
     */
    @Override
    public void insert(PostAddNewDTO postAddNewDTO) {
        log.info("执行PostServiceImpl-insert方法");
        // 获取用户id
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long userid = currentUserInfo.getId();
        log.debug("调试：PostServiceImpl-insert-获取当前用户id:{}",userid);
        // 创建帖子对象并赋值
        Post post = new Post();
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY年MM月dd日");
        String format = dateFormat.format(date);
        if (postAddNewDTO.getTitle()==null||"".equals(postAddNewDTO.getTitle())){
            postAddNewDTO.setTitle(format+"记");
        }
        post.setTitle(postAddNewDTO.getTitle());
        post.setUserId(userid);
        if (postAddNewDTO.getTopicId()==null){
            postAddNewDTO.setTopicId(1L);
        }
        post.setTopicId(postAddNewDTO.getTopicId());
        log.debug("调试：PostServiceImpl-insert-post:{}",post);
        if (postAddNewDTO.getVisible()!=null){
            post.setVisible(postAddNewDTO.getVisible());
        }else {
            post.setVisible(1);
        }
        // 插入帖子
        int rows = postMapper.insert(post);
        log.info("执行插入帖子方法");
        //帖子插入失败
        if (rows != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "帖子发布失败，服务器忙(" +
                    State.ERR_SERVER_INSERT.getValue() + "),请稍后再次尝试!");
        }

        // 创建帖子文本对象，赋值
        PostText postText = new PostText();
        postText.setPostText(postAddNewDTO.getPostText());
        postText.setPostId(post.getId());
        log.debug("调试：PostServiceImpl-insert-postText:{}",postText);

        //插入帖子文本
        rows = postTextMapper.insert(postText);
        log.info("执行插入帖子文本方法");
        if (rows != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "帖子发布失败，服务器忙(" +
                    State.ERR_SERVER_INSERT.getValue() + "),请稍后再次尝试!");
        }
        List<String> post_img_urls = postAddNewDTO.getImgUrls();
        log.debug("服务层-写日记，前端提交的图片名列表值：{}",post_img_urls);
        // 插入帖子图片  见下面方法给post_img_urls赋值
            if (post_img_urls.size() > 0) {
                imgMapper.insert(post.getId(), post_img_urls);
            }else{
                throw new ServiceException(State.ERR_IMG,"请至少添加一张图片");
            }

    }

    /**
     * @param postIds 所删除的帖子的id
     */
    @Override
    public void deleteById(List<Long> postIds) {
        for (Long id : postIds) {
            postMapper.deleteById(id);
            postTextMapper.deleteByPostId(id);
            imgMapper.delete(id);
            log.debug(id+"这个id的帖子已被删除");
        }
    }


    /**
     * 上传帖子图片
     * 当用户发起写日记请求后再将所有的图片及对应的帖子id插入到数据库post_img表中
     */
    @Override
    public String uploadImg(MultipartFile picFile) {
        String imgUrl = Upload.upload(dirPath, picFile);
        log.debug("上传处理后返回给前端的图片名：{}",imgUrl);
        return imgUrl;
    }

    /**
     * 移除图片
     * @param name
     * @return
     */
    @Override
    public int removeImg(String name) {
        return Upload.remove(dirPath, name);
    }

    /**
     * 根据帖子id获取详细信息
     */
    @Override
    public PostDetailVO getDetail(Long postId) {
        PostDetailVO detail = postMapper.getDetail(postId);
        setPostDetails(detail);
        log.info("调试:postMapper.getDetail(postId)值为：{}",detail );
        return detail;
    }

    /**
     * 根据最新时间显示帖子
     */
    @Override
    public List<PostDetailVO> getPostsByTime(Integer page) {

        Page postPage = getPostPage(page);
        Integer offset = postPage.getSkipPage();
        Integer count = postPage.getPageSize();

        log.debug("调试：PostServiceImpl-getPosts-byTime-offset:{},count:{}", offset, count);
        List<PostDetailVO> postsByTime = postMapper.getPostsByTime(offset, count);
        setPostDetails(postsByTime);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", postsByTime);
        return postsByTime;

    }

    /**
     * 精选
     */
    @Override
    public List<PostDetailVO> getPostsByLike(Integer page) {
        Page postPage = getPostPage(page);
        Integer offset = postPage.getSkipPage();
        Integer count = postPage.getPageSize();

        List<PostDetailVO> postsByLike = postMapper.getPostsByLike(offset, count);
        setPostDetails(postsByLike);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", postsByLike);
        return postsByLike;
    }
    /**
     * 根据最新时间显示帖子
     */
    @Override
    public List<PostDetailVO> getPostsByTimePage(PageSimpleDTO page) {
        log.debug("调试：PostServiceImpl-getPosts-byTime-page:{}", page);
        List<PostDetailVO> postsByTime = postMapper.getPostsByTimePage(page);
        setPostDetails(postsByTime);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", postsByTime);
        return postsByTime;

    }

    /**
     * 精选
     */
    @Override
    public List<PostDetailVO> getPostsByLikePage(PageSimpleDTO page) {
        System.out.println("PostServiceImpl.getPostsByLike");
        System.out.println("page = " + page);
        List<PostDetailVO> postsByLike = postMapper.getPostsByLikePage(page);
        setPostDetails(postsByLike);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", postsByLike);
        return postsByLike;
    }

    @Override
    public void setPostDetails(PostDetailVO postDetailVO) {
        if (postDetailVO == null) {
            return;
        }
        // 注入文本
        postDetailVO.setPostText(postTextMapper.getPostTextByPostId(postDetailVO.getId()));
        // 注入用户信息
        postDetailVO.setUser(userMapper.queryUserSimpleInfoById(postDetailVO.getUserId()));
        // 注入图片
        postDetailVO.setImgUrl(imgMapper.queryImgsUrlByPostsId(postDetailVO.getId()));
        // 注入话题信息
        postDetailVO.setTopic(topicMapper.getSimpleInfoById(postDetailVO.getTopicId()));
    }

    @Override
    public void setPostDetails(List<PostDetailVO> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        for (PostDetailVO details : list) {
            // 注入文本
            details.setPostText(postTextMapper.getPostTextByPostId(details.getId()));
            // 注入用户信息
            details.setUser(userMapper.queryUserSimpleInfoById(details.getUserId()));
            // 注入图片
            details.setImgUrl(imgMapper.queryImgsUrlByPostsId(details.getId()));
            // 注入话题信息
            details.setTopic(topicMapper.getSimpleInfoById(details.getTopicId()));
        }
    }

    /**
     * 获取同一话题的帖子
     * @param topicId
     * @return
     */
    @Override
    public List<PostDetailVO> getPostsByTopic(Long topicId) {
        List<PostDetailVO> posts = postMapper.getPostsByTopicId(topicId);
        setPostDetails(posts);
        return posts;
    }

    @Override
    public List<PostDetailVO> getUserAllPost() {
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long id = currentUserInfo.getId();
        List<PostDetailVO> userAllPost = postMapper.getUserAllPost(id);
        setPostDetails(userAllPost);
        return userAllPost;
    }


    @Override
    public void likeAdd(Long postId) {
        log.debug("调试:帖子点赞:id={}", postId);
        postMapper.likeAdd(postId);
    }

    @Override
    public void viewAdd(Long postId) {
        log.debug("调试:帖子浏览:id={}", postId);
        postMapper.viewAdd(postId);
    }

    @Override
    public void commentAdd(Long postId) {
        log.debug("调试:帖子评论增加:id={}", postId);
        postMapper.commentAdd(postId);
    }

    @Override
    public Long countPostsByTopic(Long topicid) {
        return postMapper.countPostsByTopic(topicid);
    }

    /**
     * 通过页码设置页的属性，比如此页的帖子数量有多少
     * 流程：
     * ①用户点击第几页或者点击下一页，将页码传到controller，
     * ②controller调用业务方法，并把页码当参数传递，
     * ③对应业务方法调用此方法，首页设置页码的属性，：数据里的帖子总数，当前页面，并计算当前页含有多少条帖子
     */

    @Override
    public Page getPostPage(Integer page) {
        return getPostPage(page, 5);
    }

    @Override
    public Page getPostPage(Integer page, Integer pageSize) {
        Page postPage = new Page();
        // 设置当前页码
        postPage.setCurrentPage(page);
        // 设置帖子每页多少条
        postPage.setPageSize(pageSize);
        return postPage;
    }

    @Override
    public Long getTotal() {
       return postMapper.countAll();
    }

    @Override
    public List<PostDetailVO> getUserAllPostById(Long id) {
        getCurrentUser();
        List<PostDetailVO> userAllPost = null;
        if (currentUserInfo != null) {
            if (friendService.isFriend(id)){
                userAllPost = postMapper.getUserPublicAndFriendPost(id);
            }else {
                userAllPost = postMapper.getUserPublicPost(id);
            }
        }else {
            userAllPost = postMapper.getUserPublicPost(id);
        }
        setPostDetails(userAllPost);
        return userAllPost;
    }


    @Override
    public void setPostState(List<Long> postIds) {
        for (Long postId : postIds) {
            postMapper.setPostState(postId);
        }
    }

    @Override
    public void cancelCheck(List<Long> postIds) {
        for (Long postId : postIds) {
            postMapper.cancelCheck(postId);
        }
    }



    @Override
    public List<PostDetailVO> getNewTimeNonCheckedPosts(PageSimpleDTO page) {

        List<PostDetailVO> newTimeNonCheckedPosts = postMapper.getNewTimeNonCheckedPosts(page);
        setPostDetails(newTimeNonCheckedPosts);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", newTimeNonCheckedPosts);
        return newTimeNonCheckedPosts;
    }


    @Override
    public List<PostDetailVO> getNonCheckedPostsByOldTimeAndPageCount(PageSimpleDTO page) {
        List<PostDetailVO> oldTimeNonCheckedPosts = postMapper.getOldTimeNonCheckedPosts(page);
        setPostDetails(oldTimeNonCheckedPosts);
        log.debug("调试：PostServiceImpl-getPosts-byTime:{}", oldTimeNonCheckedPosts);
        return oldTimeNonCheckedPosts;
    }

    @Override
    public Long countAllNonCheck() {
        Long aLong = postMapper.countAllNonCheck();
        log.debug("调试：PostServiceImpl-countAllNonCheck未审核帖子的条数：{}",aLong);
        return aLong;
    }

    @Override
    public List<PostDetailVO> getAllPosts(PageSimpleDTO page) {
        List<PostDetailVO> allPosts = postMapper.getAllPosts(page);
        setPostDetails(allPosts);
        return allPosts;
    }

    @Override
    public Long countAllPosts() {
        return postMapper.countAllPosts();
    }

    @Override
    public List<PostDetailVO> getAllPostsChecked(PageSimpleDTO page) {
        List<PostDetailVO> allPostsChecked = postMapper.getAllPostsChecked(page);
        setPostDetails(allPostsChecked);
        return allPostsChecked;
    }

    @Override
    public Long countAllPostsChecked() {
        return postMapper.countAllPostsChecked();
    }
}
