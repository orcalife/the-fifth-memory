package cn.orca.thefifthmemory.server.service.impl;

import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.mapper.common.FriendMapper;
import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.service.IFriendService;
import cn.orca.thefifthmemory.server.service.IUserService;
import cn.orca.thefifthmemory.server.web.State;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FriendServiceImpl implements IFriendService {
    @Autowired
    private FriendMapper friendMapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private IUserService userService;

    /*查询首页好友列表
    根据当前用户(session)id查询到所有的好友id,关联查询到好友信息(昵称,签名,头像)
    操作表:好友表,用户表
    返回查询到的所有好友*/

    private UserInformationVo currentUserInfo;
    /**
     * 获取当前用户信息
     */
    public void getCurrentUser(){
        currentUserInfo = userService.getCurrentUserInfo(request);
    }


    public List<FriendSimpleInfoVO> showFriends() {
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long userid = currentUserInfo.getId();
        log.debug("当前用户:" + userid);
        List<Long> list = friendMapper.queryAllFriendId(userid);
        log.trace(String.valueOf(list));

        List<FriendSimpleInfoVO> userList = new ArrayList<>();

        for (Long f : list) {
            userList.add(userService.queryFriendInfo(f));
        }
        log.debug(String.valueOf(userList.size()));
        log.debug("返回的用户列表:" + userList.toString());
        return userList;
    }


    public void addFriend(Long friendId) {
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long userId = currentUserInfo.getId();
        log.debug("当前用户id{},要添加的好友id{}", userId, friendId);
        if (friendId.equals(userId)){
            throw new ServiceException(State.ERR_OWN_FRIEND,"不能添加自己");
        }
        Boolean friend = isFriend(friendId);
        if (friend==false){
            friendMapper.addFriend(userId, friendId);
            friendMapper.addFriend(friendId, userId);
        }else {
            throw new ServiceException(State.ERR_OWN_FRIEND,"你们已是好友，请勿重复添加");
        }
    }

    @Override
    public void deleteFriend(Long friendId) {
        getCurrentUser();
        if (currentUserInfo == null) {
            throw new ServiceException(State.ERR_NO_LOGIN,"请先登录");
        }
        Long userid = currentUserInfo.getId();
        friendMapper.deleteFriend(userid,friendId);
        friendMapper.deleteFriend(friendId,userid);
    }

    @Override
    public Boolean isFriend(Long friendId) {
        getCurrentUser();
        Long userId = currentUserInfo.getId();
        int mapperFriend = friendMapper.isFriend(userId, friendId);
        if (mapperFriend>0){
            return true;
        }
        return false;
    }
}
