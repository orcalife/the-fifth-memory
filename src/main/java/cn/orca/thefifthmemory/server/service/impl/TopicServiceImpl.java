package cn.orca.thefifthmemory.server.service.impl;

import cn.orca.thefifthmemory.server.entity.Topic;
import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.mapper.common.TopicMapper;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.TopicDTO;
import cn.orca.thefifthmemory.server.pojo.vo.AllTopicVO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import cn.orca.thefifthmemory.server.service.IPostService;
import cn.orca.thefifthmemory.server.service.ITopicService;
import cn.orca.thefifthmemory.server.util.Upload;
import cn.orca.thefifthmemory.server.web.State;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Service
public class TopicServiceImpl implements ITopicService {
    @Autowired
    private IPostService postService;
    @Autowired
    private TopicMapper topicMapper;
    private String dirPath = "D:/Life-Images/Topic-Images/";

    @Override
    public String uploadImg(MultipartFile picFile) {
        String imgUrl = Upload.upload(dirPath, picFile);
        log.debug("话题服务层-uploadImg方法-imgUrl值：{}",imgUrl);
        return imgUrl;
    }

    /**
     * 移除图片
     * @param name
     * @return
     */
    @Override
    public int removeImg(String name) {
        return Upload.remove(dirPath, name);
    }

    @Override
    public List<AllTopicVO> getAllTopicByPage(PageSimpleDTO page) {
        List<AllTopicVO> allTopic = topicMapper.getAllTopicByPage(page);
//        Long count = getCountPostByTopicId();
        for (AllTopicVO topic : allTopic) {
            Long id = topic.getId();
            List<PostDetailVO> postsByTopic = postService.getPostsByTopic(id);
            Long count  = (long) postsByTopic.size();
            topic.setPostsNumber(count);
        }
        return allTopic;
    }
    @Override
    public List<AllTopicVO> getAllTopic() {
        List<AllTopicVO> allTopic = topicMapper.getAllTopic();
//        Long count = getCountPostByTopicId();
        for (AllTopicVO topic : allTopic) {
            Long id = topic.getId();
            List<PostDetailVO> postsByTopic = postService.getPostsByTopic(id);
            Long count  = (long) postsByTopic.size();
            topic.setPostsNumber(count);
        }
        return allTopic;
    }

    /*private Long getCountPostByTopicId() {
        return postService.getCountPostByTopicId();
    }*/

    @Override
    public List<PostDetailVO> getAllPostByTopic(Long topicId) {
        List<PostDetailVO> posts = postService.getPostsByTopic(topicId);
        log.debug("TopicServiceImpl-getAllPostByTopic-posts:{}",posts);
        return posts;
    }

    @Override
    public void addNewTopic(TopicDTO topicDTO) {
        String name = topicDTO.getTopicName();
        log.info("执行添加话题");
        TopicDTO queryResult = topicMapper.getByName(name);
        if(queryResult!=null){
            throw new ServiceException(State.ERR_SERVER_INSERT,"添加话题失败，" +
                    "话题名称已存在！");
        }
        Topic topic = new Topic();
        //调用BeanUtils.copyProperties()将参数对象中的属性值复制到Topic对象中
        //BeanUtils.copyProperties(topicDTO,topic);
        topic.setTopicName(topicDTO.getTopicName());
        List<String> topicImg = topicDTO.getImgUrls();
        for (int i = 0; i < topicImg.size()-2; i++) {
            removeImg(topicImg.get(i));
        }
        topic.setTopicImg(topicImg.get(topicImg.size()-1));
        topic.setStatus(1);//默认状态为启用
        topicMapper.insertNewTopic(topic);
        log.info("执行结束");

    }
    @Override
    public List<Topic> getAllUseTopic() {
        List<Topic> allUseTopic = topicMapper.getAllUseTopic();
        return allUseTopic;
    }

    @Override
    public List<Topic> getAllNoUseTopic() {
        List<Topic> allNoUseTopic = topicMapper.getAllNoUseTopic();
        return allNoUseTopic;
    }

    //将禁用话题状态改为启用
    @Override
    public int updateTopiToEnable(Long id) {
        log.info("执行修改话题状态");
        int row = topicMapper.updateStatusById(id, 1);
        return row;
    }
    //将禁用话题状态改为启用
    @Override
    public int updateTopiToDisable(Long id) {
        log.info("执行修改话题状态");
        int row = topicMapper.updateStatusById(id, 0);
        log.info("执行结束");
        return row;
    }

    @Override
    public void updateTopic(TopicDTO topicDTO) {
        if (topicDTO.getId() == null) {
            throw new ServiceException(State.ERR_ID_NULL,"你没有选择要更新的话题");
        }
        int j = topicDTO.getImgUrls().size() - 1;
        topicDTO.setTopicImg(topicDTO.getImgUrls().get(j));
        topicMapper.updateTopic(topicDTO);
        List<String> topicImg = topicDTO.getImgUrls();
        for (int i = 0; i < topicImg.size()-2; i++) {
            removeImg(topicImg.get(i));
        }
    }

    @Override
    public Long countAllTopic() {
        return topicMapper.countAllTopic();
    }

    @Override
    public void setTopicStatus(Long id, Integer status) {
        Integer state ;
        if (status == 1){
            state=2;
        }else {
            state=1;
        }
        topicMapper.setTopicStatus(id,state);
    }
}
