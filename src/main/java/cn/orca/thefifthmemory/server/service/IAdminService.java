package cn.orca.thefifthmemory.server.service;


import cn.orca.thefifthmemory.server.pojo.dto.AdminLoginDTO;

public interface IAdminService {

    String login(AdminLoginDTO adminLoginDTO);

}
