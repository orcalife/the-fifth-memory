package cn.orca.thefifthmemory.server.service;


import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;

import java.util.List;

public interface IFriendService {
    List<FriendSimpleInfoVO> showFriends();
    void addFriend(Long friendId);

    void deleteFriend(Long friendId);

    Boolean isFriend(Long friendId);
}
