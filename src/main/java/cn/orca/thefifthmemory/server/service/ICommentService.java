package cn.orca.thefifthmemory.server.service;


import cn.orca.thefifthmemory.server.pojo.dto.CommentAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.CommentReplyAddDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PageCommentDTO;
import cn.orca.thefifthmemory.server.pojo.vo.CommentVO;
import cn.orca.thefifthmemory.server.util.Page;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ICommentService {
    /*
     * 添加让方法具有事务性*/
    @Transactional
    void addComment(CommentAddDTO comment);
    @Transactional
    void addReply(CommentReplyAddDTO comment);
    @Transactional
    void deleteComment(Long commentId);
    List<CommentVO> listAllComments(Long postId, int page);

    List<CommentVO> listAllCommentsPage(PageCommentDTO page);

    List<CommentVO> listReplyByCommentId(Long id);



}
