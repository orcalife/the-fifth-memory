package cn.orca.thefifthmemory.server.service.impl;
import cn.orca.thefifthmemory.server.entity.User;
import cn.orca.thefifthmemory.server.exception.ServiceException;
import cn.orca.thefifthmemory.server.mapper.common.UserMapper;
import cn.orca.thefifthmemory.server.pojo.dto.*;
import cn.orca.thefifthmemory.server.pojo.vo.FriendSimpleInfoVO;
import cn.orca.thefifthmemory.server.pojo.vo.UserInformationVo;
import cn.orca.thefifthmemory.server.pojo.vo.UserSimpleInfoOfPostOrCommentVO;
import cn.orca.thefifthmemory.server.repository.UserRedisRepository;
import cn.orca.thefifthmemory.server.service.IUserService;
import cn.orca.thefifthmemory.server.util.JWTUtil;
import cn.orca.thefifthmemory.server.util.MD5Util;
import cn.orca.thefifthmemory.server.util.Page;
import cn.orca.thefifthmemory.server.util.Upload;
import cn.orca.thefifthmemory.server.web.State;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
public
class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private EmailServiceImpl emailServiceImpl;
    //存储注册用户的完整信息,为存入数据库准备
    private User serve_user;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRedisRepository userRedisRepository;
    @Autowired
    private HttpServletRequest request;

    private UserInformationVo userInformationVo;
    private String dirPath = "D:/Life-Images/Header-Images/";

    @Autowired
    private AuthenticationManager authenticationManager;
//    @Value("${sys.defaultuseravater}")
    private String default_user_avater="header_default.jpg";
    private String signature = "这个家伙很懒，什么都没有留下";



    public void sendActiveCode(String email) {
        log.info("服务层-sendActiveCode方法执行");
        if (userMapper.queryEmail(email) != null) {
            throw new ServiceException(State.ERR_EMAIL_EXITS, "该邮箱已注册过");
        }
        serve_user = new User();
        serve_user.setEmail(email);
        String activeCode = MD5Util.generateUUID().substring(0, 5);
        serve_user.setActiveCode(activeCode);
        serve_user.setAccountStatus(0);
        emailServiceImpl.sendMail(email, "欢迎加入ORCA LIFE社区", "亲爱的第9756位小鲨鱼,欢迎你加入LIFE社区,你的激活码为" + activeCode +
                "快去完成注册吧");
    }

    public void register(UserRegisterDTO userRegisterDTO) {
        log.debug("控制层-register执行-参数UserRegisterDTO值为：{}",userRegisterDTO);
        if (!(serve_user.getActiveCode().equals(userRegisterDTO.getActiveCode()))) {
            throw new ServiceException(State.ERR_ACTIVECODE, "验证码错误");
        }
        if (!(serve_user.getEmail().equals(userRegisterDTO.getEmail()))) {
            throw new ServiceException(State.ERR_EMAIL_DIFFERENT, "邮箱前后不一致");
        }
//        TODO 后期可能增加其他注册方式，比如 QQ，此时的用户名就要换成QQ
        serve_user.setUsername(userRegisterDTO.getEmail());
        serve_user.setNick(userRegisterDTO.getNick());
        serve_user.setAccountStatus(1);
        String encodePassword = passwordEncoder.encode(userRegisterDTO.getPassword());
        serve_user.setPassword(encodePassword);
        serve_user.setAvatar(default_user_avater);
        serve_user.setSignature(signature);
        log.debug("用户服务层-用户注册完整信息serve_user值：{}",serve_user);
        log.info("服务层将完整的用户信息插入到数据库");
        insert(serve_user);
    }

    @Override
    public FriendSimpleInfoVO queryFriendInfo(Long friendId) {
        return userMapper.queryFreindSimpleInfoById(friendId);
    }

    public void insert(User user) {
        int rows = userMapper.insert(user);
        if (rows != 1) {
            throw new ServiceException(State.ERR_SERVER_INSERT, "注册失败，服务器忙，请稍后再试");
        }
    }


    public String login(UserLoginDTO userLoginDTO) {
        log.debug("服务层-login方法执行-参数userLoginDTO值w为：{}",userLoginDTO);
        // 准备被认证数据
        Authentication authentication
                = new UsernamePasswordAuthenticationToken(
                userLoginDTO.getUsername(),userLoginDTO.getPassword());
        // 调用AuthenticationManager验证用户名与密码
        // 执行认证，如果此过程没有抛出异常，则表示认证通过，如果认证信息有误，将抛出异常
        log.info("authenticationManager值为:"+authenticationManager);
        Authentication authenticate = authenticationManager.authenticate(authentication);
        // 如果程序可以执行到此处，则表示登录成功
        // 生成此用户数据的JWT
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authenticate.getPrincipal();
        Map<String, Object> claims = new HashMap<>();
        claims.put("permissions", JSON.toJSONString(user.getAuthorities()));
        claims.put("username",user.getUsername());
        String jwt = JWTUtil.GenerateJwt(claims);
        /*TODO 考虑是否将登录的用户信息缓存到redis*/
        userInformationVo = userMapper.queryUserInfoByUsername(user.getUsername());
        Integer status = userMapper.queryUserAccountStatus(user.getUsername());
        if (status==2){
            throw new ServiceException(State.ERR_USER_STATUS,"该用户已被禁用，请联系管理员进行处理");
        }
        userRedisRepository.save(userInformationVo);
        // 返回JWT数据
        return jwt;
    }


    /**
     * 获取当前用户信息
     * @return
     */
    public UserInformationVo getCurrentUserInfo(HttpServletRequest request) {
        log.info("getCurrentUserInfo方法执行");
        String userName = JWTUtil.getUserName(request);
        log.debug("getCurrentUserInfo-userName值为；{}",userName);
        UserInformationVo details = userRedisRepository.getDetailsByUsername(userName);
        return details;
    }

    @Override
    public void updatePersonalInfo(UserUploadInfoDTO userUploadInfoDTO) {
        String userName = JWTUtil.getUserName(request);
        userUploadInfoDTO.setUserName(userName);
        int i = userMapper.updateUserInfo(userUploadInfoDTO);
        if (i!=1){
            throw new ServiceException(State.ERR_SERVER_UPDATE,"更新出错了");
        }
        userInformationVo = userMapper.queryUserInfoByUsername(userName);
        userRedisRepository.save(userInformationVo);
    }


    /**
     * 通过帖子详情页的postid查询到帖子作者的userid
     * 通过userID查询到作者相关信息
     * @param userid
     * @return
     */
    public UserSimpleInfoOfPostOrCommentVO getUserSimpleInfo(Long userid) {
        UserSimpleInfoOfPostOrCommentVO simpleInfo=userMapper.queryUserSimpleInfoById(userid);
        return simpleInfo;
    }




    @Override
    public String uploadImg(MultipartFile picFile) {
        String imgUrl = Upload.upload(dirPath, picFile);
        log.debug("用户服务层-uploadImg方法-imgUrl值：{}",imgUrl);
        return imgUrl;
    }

    @Override
    public void uploadSave(List<String> imgUrls) {
        String imgUrl = null;
        if (imgUrls.size()>0) {
            for (int i = 0; i < imgUrls.size()-2; i++) {
                Upload.remove(dirPath,imgUrls.get(i));
            }
            imgUrl = imgUrls.get(imgUrls.size()-1);
            log.debug("用户服务层-uploadSave方法-imgUrl值：{}",imgUrl);
            String userName = JWTUtil.getUserName(request);
            int i = userMapper.updateAvatar(imgUrl, userName);
            log.debug("用户服务层-uploadSave方法执行sql1语句更新");
            if (i!=1){
                throw new ServiceException(State.ERR_SERVER_UPDATE,"更新出错了");
            }
            userInformationVo = userMapper.queryUserInfoByUsername(userName);
            userRedisRepository.save(userInformationVo);
            String avatar = getCurrentUserInfo(request).getAvatar();
            Upload.remove(avatar,dirPath);
        }
    }


//    TODO 用户信息缓存预热
    @Override
    public void preloadCache() {

    }

    /**
     * 修改密码
     * @param updateUserPassword
     */
    @Override
    public void updatePassword(UpdateUserPassword updateUserPassword) {
        String username = JWTUtil.getUserName(request);
        updateUserPassword.setUsername(username);
        String password = userMapper.queryPassword(username);
        boolean matches = passwordEncoder.matches(updateUserPassword.getOldPassword(), password);
        if (matches==false){
            throw new ServiceException(State.ERR_SERVER_UPDATE,"原密码错误");
        }
        String encode = passwordEncoder.encode(updateUserPassword.getNewPassword());
        updateUserPassword.setNewPassword(encode);
        int i = userMapper.updatePassword(updateUserPassword);
        if (i!=1){
            throw new ServiceException(State.ERR_SERVER_UPDATE,"更新出错了");
        }
    }

    @Override
    public void logout() {
        /*jwt生效  springsecurity上下文清空*/

    }

    @Override
    public UserInformationVo userInfoById(Long id) {
        return userMapper.userInfoById(id);
    }

    /*忘记密码修改密码*/
    @Override
    public void forgetPassword(ForgetPasswordDTO forgetPasswordDTO) {

        log.debug("修改密码参数:{}", forgetPasswordDTO);
        if (!(serve_user.getActiveCode().equals(forgetPasswordDTO.getActiveCode()))) {
            throw new ServiceException(State.ERR_ACTIVECODE, "验证码错误");
        }
        if (!(serve_user.getEmail().equals(forgetPasswordDTO.getEmail()))) {
            throw new ServiceException(State.ERR_EMAIL_DIFFERENT, "邮箱前后不一致");
        }
        String encodePassword = passwordEncoder.encode(forgetPasswordDTO.getNewPassword());
        userMapper.forgetPassword(serve_user.getEmail(), encodePassword);
        log.info("密码修改成功");

    }

    @Override
    public List<UserInformationVo> getAllUser(PageSimpleDTO page) {
        List<UserInformationVo> allUsers = userMapper.getAllUser(page);
        return allUsers;
    }

    @Override
    public Long countAllUser() {
        Long count = userMapper.countAllUser();
        return count;
    }

    @Override
    public UserInformationVo searchUser(String email) {
        return userMapper.searchUser(email);
    }

    @Override
    public void setUserStatus(Long id, Integer accountStatus) {
        Integer state ;
        if (accountStatus == 1){
            state=2;
        }else {
            state=1;
        }
        userMapper.setUserStatus(id,state);
    }


    /*忘记密码发送验证码*/
    public void sendActiveCodeForgetPassword(String email) {
        log.info("服务层-sendActiveCodeByEmail方法执行");
        if (userMapper.queryEmail(email) == null) {
            throw new ServiceException(State.ERR_ACCOUNT_NOT_EXITS, "该邮箱还没注册过");
        }
        serve_user = new User();
        serve_user.setEmail(email);
        String activeCode = MD5Util.generateUUID().substring(0, 5);
        serve_user.setActiveCode(activeCode);
        emailServiceImpl.sendMail(email, "请接收您的验证码", activeCode);
    }


}
