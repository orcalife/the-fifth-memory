package cn.orca.thefifthmemory.server.service;

import cn.orca.thefifthmemory.server.entity.Post;
import cn.orca.thefifthmemory.server.pojo.dto.PageSimpleDTO;
import cn.orca.thefifthmemory.server.pojo.dto.PostAddNewDTO;
import cn.orca.thefifthmemory.server.pojo.vo.PostDetailVO;
import cn.orca.thefifthmemory.server.util.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


public interface IPostService {
    /**
     * 插入帖子
     * @param
     * @param postAddNewDTO 帖子内容
     */
    @Transactional
    void insert(PostAddNewDTO postAddNewDTO);

    /**
     * 根据帖子id删除帖子
     * @param postIds 所删除的帖子的id
     */
    void deleteById(List<Long> postIds);

    /**
     * 上传图片
     */
    String uploadImg(MultipartFile picFile);

    /**
     * 移除图片
     */
    int removeImg(String name);

    PostDetailVO getDetail(Long postId);

    List<PostDetailVO> getPostsByTimePage(PageSimpleDTO page);

    List<PostDetailVO> getPostsByLikePage(PageSimpleDTO page);

    List<PostDetailVO> getPostsByTime(Integer page);

    List<PostDetailVO> getPostsByLike(Integer page);


    /**
     * 补全每一个帖子的文本，图片，用户信息
     */
    void setPostDetails(PostDetailVO postDetailVO);
    void setPostDetails(List<PostDetailVO> list);

    /**
     * 获取同一话题下的所有帖子
     * @param topicId
     * @return
     */
    List<PostDetailVO> getPostsByTopic(Long topicId);

    List<PostDetailVO> getUserAllPost();

    /*void preLoadCache();*/

    void likeAdd(Long postId);

    void viewAdd(Long postId);

    void commentAdd(Long postId);

//  添加话题下的帖子数量
    Long countPostsByTopic(Long topicid);

    Page getPostPage(Integer page);
    Page getPostPage(Integer page, Integer pageSize);

    /**
     * 获取总页数
     */
    Long getTotal();

    List<PostDetailVO> getUserAllPostById(Long id);

    /**
     * 通过页码获取最新未审核帖子
     * @param page 页码
     * @return 返回页码的最旧未审核帖子
     */
    List<PostDetailVO> getNewTimeNonCheckedPosts(PageSimpleDTO page);



    void setPostState(List<Long> postIds);

    void cancelCheck(List<Long> postIds);

    /**
     * 通过页码获取最旧未审核帖子
     * @param
     * @return 返回页码的最旧未审核帖子
     */
    List<PostDetailVO> getNonCheckedPostsByOldTimeAndPageCount(PageSimpleDTO page);

    Long countAllNonCheck();

    List<PostDetailVO> getAllPosts(PageSimpleDTO page);

    Long countAllPosts();

    List<PostDetailVO> getAllPostsChecked(PageSimpleDTO page);

    Long countAllPostsChecked();

}
