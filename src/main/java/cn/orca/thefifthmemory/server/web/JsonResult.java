package cn.orca.thefifthmemory.server.web;

import lombok.Data;

import java.io.Serializable;

@Data
public class JsonResult<T> implements Serializable {

    private Integer state;

    private String message;

    private Long count;

    private T data;

    private JsonResult() {}

    public static JsonResult<Void> ok() {
        return ok(null);
    }

    public static <T> JsonResult<T> ok(T data) {
        JsonResult<T> jsonResult = new JsonResult<T>();
        jsonResult.setState(State.OK.getValue());
        jsonResult.setData(data);
        return jsonResult;
    }


    public static <T> JsonResult<T> ok(T data,String message) {
        JsonResult<T> jsonResult = new JsonResult<T>();
        jsonResult.setState(State.OK.getValue());
        jsonResult.setData(data);
        jsonResult.setMessage(message);
        return jsonResult;
    }
 public static <T> JsonResult<T> ok(T data,Long count) {
        JsonResult<T> jsonResult = new JsonResult<T>();
        jsonResult.setState(State.OK.getValue());
        jsonResult.setData(data);
        jsonResult.setCount(count);
        return jsonResult;
    }

    public static JsonResult<Void> fail(State state, String message) {
        JsonResult<Void> jsonResult = new JsonResult<Void>();
        jsonResult.setState(state.getValue());
        jsonResult.setMessage(message);
        return jsonResult;
    }

}
