package cn.orca.thefifthmemory.server.web;

public enum State {

    OK(20000),
    ERR_SERVER_INSERT(50000),//服务器插入错误
    ERR_SERVER_UPDATE(50060),
    ERR_SERVER(50050),//服务器出错了
    ERR_SERVER_NULLPOINTEREXCEPTION(50020),//空指针异常
    ERR_INTERNAL_SERVER_ERROR(50100) ,
    ERR_SERVER_SELECT(50010),//服务器查询异常
    ERR_USER_STATUS(50300),//用户状态异常
    ERR_BAD_REQUEST(40000), // 请求参数格式错误
    ERR_ACCOUNT_NOT_EXITS(40400),//账号不存在
    USERNAME_OR_PASSWORD_ERROR(40800),
    ERR_PERMISSION_ERROR(40900),
    ERR_EMAIL_EXITS(40200),//邮箱已经存在
    ERR_EMAIL_DIFFERENT(40201),//邮箱前后不一致
    ERR_PASSWORD(40400),//密码错误
    ERR_ACTIVECODE(40300),//验证码错误
    ERR_JWT_EXPIRED(40900), // 客户端引起的--JWT--过期
    ERR_JWT_MALFORMED(40901), // 客户端引起的--JWT--数据无效
    ERR_JWT_SIGNATURE(40902), // 客户端引起的--JWT--签名错误
    ERR_COMMENT_NOT_FOUND(40401),//评论不存在
    ERR_IMG(40403),
    ERR_NO_LOGIN(40505),//未登录
    ERR_ID_NULL(40101),
    ERR_OWN_FRIEND(40402);//不能添加自己

    private Integer value;

    State(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
